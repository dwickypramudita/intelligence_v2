LIMIT_PENGERJAAN = 10000; 			//waktu pengerjaan per soal dalam ms -> ada di semua subtest
TIMELIMIT_MEMORIZATION	= 10000 	//waktu soal hapalan tampiil dalam ms -> ada khusus di building memory
COUNTER_PREPARATION = 5				//waktu countdown sebelum mengerjakan soal building memory -> ada khusus di building memory


//3D ROTATION ================================================================================================================================
//TRIAL
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_1_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_1a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1d.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_1e.png","answer":false}]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"trial/trial_2.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_2_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_2a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2b.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_2c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2d.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2e.png","answer":false}]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":true}]
	},
	{
	 "level" : "medium",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":false}]
	},
	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":false}]
	}
];

//LINE COMBINATION ============================================================================================================================
//TRIAL
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_1_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_1a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1d.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_1e.png","answer":false}]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"trial/trial_2.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_2_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_2a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2d.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_2e.png","answer":false}]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":true},{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":false}]
	},
	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":false}]
	}
];

//PAPER FOLDING ===============================================================================================================================
//TRIAL
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_1_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_1a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1d.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1e.png","answer":true}]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"trial/trial_2.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_2_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_2a.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_2b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2d.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2e.png","answer":false}]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":true},{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":true}]
	},
	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":false}]
	},
];

//BUILDING MEMORY  ============================================================================================================================
//TRIAL
var ARRAY_QUESTION = [
	{
		 "level" : "trial",
		 "sequence" : "1",
		 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
		 "frame" : URL_IMAGE_PATH+"trial/trial_1_frame.png",
		 "question" : 
		[
			{
			 "sub_sequence" : "1",
			 "path" : URL_IMAGE_PATH+"trial/trial_1_sub_1.png",
			 "expected" : URL_IMAGE_PATH+"trial/trial_1_sub_1_expected.png",
			 "choice" : 
			[{"path":URL_IMAGE_PATH+"a.png","answer":false},{"path":URL_IMAGE_PATH+"b.png","answer":true},{"path":URL_IMAGE_PATH+"c.png","answer":false},{"path":URL_IMAGE_PATH+"d.png","answer":false},{"path":URL_IMAGE_PATH+"e.png","answer":false}]
			},
			{
			 "sub_sequence" : "2",
			 "path" : URL_IMAGE_PATH+"trial/trial_1_sub_2.png",
			 "expected" : URL_IMAGE_PATH+"trial/trial_1_sub_2_expected.png",
			 "choice" : 
			[{"path":URL_IMAGE_PATH+"a.png","answer":true},{"path":URL_IMAGE_PATH+"b.png","answer":false},{"path":URL_IMAGE_PATH+"c.png","answer":false},{"path":URL_IMAGE_PATH+"d.png","answer":false},{"path":URL_IMAGE_PATH+"e.png","answer":false}]
			},

		]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	//easy
	{
		"level" : "easy",
		"sequence" : "1",
		"path" : URL_IMAGE_PATH+"easy/easy_1.png",
		"frame" : URL_IMAGE_PATH+"easy/easy_1_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"easy/easy_1_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":true}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"easy/easy_1_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":true},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"easy/easy_1_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false}
					]
			},
		]
	},
	//medium
	{
		"level" : "medium",
		"sequence" : "1",
		"path" : URL_IMAGE_PATH+"medium/medium_1.png",
		"frame" : URL_IMAGE_PATH+"medium/medium_1_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"medium/medium_1_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":true},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"medium/medium_1_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"medium/medium_1_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":true},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
		]
	},
	//hard
	{
		"level" : "hard",
		"sequence" : "1",
		"path" : URL_IMAGE_PATH+"hard/hard_1.png",
		"frame" : URL_IMAGE_PATH+"hard/hard_1_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"j.png","answer":false},
						{"path":URL_IMAGE_PATH+"l.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"g.png","answer":false},
						{"path":URL_IMAGE_PATH+"l.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"i.png","answer":false},
						{"path":URL_IMAGE_PATH+"k.png","answer":false}
					]
			},
			{
				"sub_sequence" : "4",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_4.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":true},
						{"path":URL_IMAGE_PATH+"h.png","answer":false},
						{"path":URL_IMAGE_PATH+"k.png","answer":false}
					]
			},
		]
	},
];

//SHAPE CLASSIFICATION  =======================================================================================================================
//TRIAL
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "expected" : URL_IMAGE_PATH+"trial/trial_1_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_1a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1c.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_1d.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1e.png","answer":false}]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "expected" : URL_IMAGE_PATH+"trial/trial_2_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_2a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2d.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2e.png","answer":true}]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":true}]
	},
	{
	 "level" : "medium",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":false}]
	},
	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":false}]
	},
];

//FIGURAL CLASSIFICATION  =======================================================================================================================
//TRIAL
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"trial/trial_1a.png","expected" : URL_IMAGE_PATH+"trial/trial_1a_expected.png","answer":2},
			{"path":URL_IMAGE_PATH+"trial/trial_1b.png","expected" : URL_IMAGE_PATH+"trial/trial_1b_expected.png","answer":1}
		]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"trial/trial_1a.png","expected" : URL_IMAGE_PATH+"trial/trial_1a_expected.png","answer":2},
			{"path":URL_IMAGE_PATH+"trial/trial_1b.png","expected" : URL_IMAGE_PATH+"trial/trial_1b_expected.png","answer":1}
		]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	//easy
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_1f.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1g.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1h.png","answer":1},
		]
	},
	//medium
	{
	 "level" : "medium",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_1f.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1h.png","answer":2},
		]
	},
	//hard
	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_1f.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_1g.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_1h.png","answer":3},
		]
	},
];

//DERET ANGKA  =======================================================================================================================
var angkaDeret 	= ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19","20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"]
var angkaDeret_pil = ["10", "11", "12", "13", "14", "15", "16", "17", "18", "19","20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "131", "132", "133", "134", "135", "136", "137", "148", "149", "150", "161", "162", "163", "164", "165", "166", "167", "168", "169", "170"]

//TRIAL
var ARRAY_QUESTION = [
	{
	 	"level" : "trial",
	 	"sequence" : "1",
	 	"pattern" : ["9","10","12","15","19","24","30"]
	},
	{
	 	"level" : "trial",
	 	"sequence" : "2",
		"pattern" : ["13","1","3","26","-3","10","52","-7","17"]
	}
];

//EASY/MEDIUM/HARD
var ARRAY_QUESTION = [
	{
	 	"level" : "easy",
	 	"sequence" : "1",
	 	"pattern" : ["18","10","11","12","16","21","27"]
	},
	{
	 	"level" : "medium",
	 	"sequence" : "1",
		"pattern" : ["2","-2","6","2","14","10","26"]
	},
	{
	 	"level" : "hard",
	 	"sequence" : "1",
		"pattern" : ["5","-4","3","8","2","9","11","8","15"]
	},	
];