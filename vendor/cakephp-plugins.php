<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'FrontEnds' => $baseDir . '/plugins/FrontEnds/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Services' => $baseDir . '/plugins/Services/'
    ]
];