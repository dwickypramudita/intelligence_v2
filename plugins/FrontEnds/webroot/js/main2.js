var level = 0;
var arrayResultStatus   	= [];			//array isinya status per kolom true / false. ( Benar semua atau ngga )
var arrayLeftInputFinal     = [];		//array isinya left input
var arrayRightInputFinal    = [];		//array isinya right input
var arrayResultFinal   		= [];			//array hasil perhitungan sistem untuk left input
var arrayLevelFinal   	= [];	
var arrayTimeElapsed 		= [];
var currentColumn = 1;

var allGenerateData  	= [];			//array tampungan hasil generate left input

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));
	
	if(time >= 10000){
		columnTimeout();
	}
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

$( document ).ready(function() {
    newPage();

    $('body').on('input','.right-input-number',function(e){
    	var rightID = parseInt($(this).attr('id').split("-")[1]);
    	var rightColumn = parseInt($(this).attr('data-column').split("-")[2]);
    	var lastInput = $(this).attr('data-last-input');

    	if(rightID % 10 == 0  ){
    		// Setelah 1 lajur diisi, maka cek hasil input , bener atau ngga
    		checkInputUserPerColumn(rightColumn);

    		if(lastInput=="true"){
    			if(rightID == 360){
    				//kalau sudah input paling terakhir maka munculin alert / pesan
		    		finishPartTwo();
		    		stopTimer();
		    	}else{
		    		//kalau sudah input terakhir pada sebuah halaman, maka ganti halaman baru
		    		var page = parseInt($(this).attr("data-page"));
			    	$('.row-page').css('display','none');
			    	$('.page-'+(page+1)).css('display','flex');
	    		}
    		}
    	}else{
    		//set left ID
    		var leftID1 = rightID+rightColumn+1;

    		//ambil array hasil generate untuk lajur ini
    		var randomNumberPerColumn = allGenerateData[rightColumn-1];

    		//isi left input sesuai urutan index array hasil generate
			$('#left-'+leftID1).val(randomNumberPerColumn[ (rightID % 10) + 1]);
    	}
    	//set readonly setelah user input
    	$('#right-'+rightID).attr('readonly', 'readonly');
    	
    	//set focus ke input selanjutnya
    	$('#right-'+(rightID+1)).focus();
	});
});

function newPage(){
	//generate untuk column 1
	generateRandomNumberPerColumn(1);

	//ambil array hasil generate di index ke 0
	var randomNumberPerColumn0 = allGenerateData[0];

	//set left input dengan nilai index 0 dan 1 dari array hasil generate
	$('#left-1').val(randomNumberPerColumn0[0]);	
	$('#left-2').val(randomNumberPerColumn0[1]);

    initializeTimer();

    $('#right-1').focus();
}

function generateRandomNumberPerColumn(column){
	//set currentColumn
	currentColumn = column;

	//tampilkan level di bagian atas lajur
	$('.span-level-'+column).html(level);
	
	//initialize
	var arrayGenerateRandomNumberPerColumn = [];
	// var hasil = 0;
	// var random_number = 0;
	
	console.log("==========================================================");
	console.log("Generate Column "+ column + " level : " + level);

	//untuk level 0, maka hasil generate harus kurang dari 10
	if(level == 0){
		//perulangan sebanyak left input dalam 1 lajur
		for(var i = 0 ; i< 11 ; i++){
			//initialize
			hasil = 0;
			random_number = 0;
			
			//jika i awal, maka generate saja, tidak perlu dihitung jumlahnya dari nilai sebelumnya
			if(i == 0){
				//generate
				random_number = Math.floor((Math.random() * 5) + 1);

				//masukan hasil generate ke array
				arrayGenerateRandomNumberPerColumn.push(random_number);
			}else{
				//lakukan perulangan kalau hasil penjumlah diatas 10 maka ulang lagi , sampai didapat hasil dibawah 10
				do {
					//generate
				    random_number = Math.floor((Math.random() * 7) + 1);

				    //hitung jumlah dengan nilai left input sebelumnya
				    hasil = arrayGenerateRandomNumberPerColumn[i-1] + random_number;
				    // console.log("column ke "+ column+ ", i ke "+ i+ " loop 1 : "+ arrayGenerateRandomNumberPerColumn[i-1] + " & " + random_number );
			  	} while (hasil >= 10);
			  	
			  	//masukan hasil generate ke array
			  	arrayGenerateRandomNumberPerColumn.push(random_number);
		  	}
		}
	}else{
		for(var i = 0 ; i < 11 ; i++){
			//initialize
			hasil = 0;
			random_number = 0;
			
			//kalau i dibawah level, maka generate number diantara 6 - 9.
			if(i < level ){
				//perulangan sampai dapat generate number diatas 6
				do {
				    random_number = Math.floor((Math.random() * 9) + 1);
			  	} while ( random_number < 6);

			  	//masukan ke array
			  	arrayGenerateRandomNumberPerColumn.push(random_number);
			//kalau i sama dengan level, maka generate angka dengan nilai kurang dari 6. Agar i berikutnya ada kesempatan buat dapat angka dengan total dibawah 10
			}else if(i == level  ){
				//perulangan sampai dapat generate number dibawah 6 dengan total jumlah dengan nilai sebelumnya yaitu diatas 10
				do {
				    random_number = Math.floor((Math.random() * 6) + 1);
				    hasil = arrayGenerateRandomNumberPerColumn[i-1] + random_number;
			  	} while ( hasil <= 10);

			  	//masukan ke array
			  	arrayGenerateRandomNumberPerColumn.push(random_number);
			//kalau i lebih dari level, maka generate angka dibawah 6, 
			}else if(i > level  ){
				//perulangan sampai dapat generate number dibawah 6 dengan total jumlah dengan nilai sebelumnya yaitu dibawah 10
				do {
				    random_number = Math.floor((Math.random() * 6) + 1);
				    hasil = arrayGenerateRandomNumberPerColumn[i-1] + random_number;
			  	} while (hasil >= 10);
			  	arrayGenerateRandomNumberPerColumn.push(random_number);
			}
		}
	}

	console.log( "Hasil Generate "+  JSON.stringify(arrayGenerateRandomNumberPerColumn));

	//masukan ke array AllGenerateData
	allGenerateData.push(arrayGenerateRandomNumberPerColumn);
}

function checkInputUserPerColumn(column){
	//stop stopwatch
	if(parseFloat($('#timer').html()) > 10){
		arrayTimeElapsed.push(10);
	}else{
		arrayTimeElapsed.push(parseFloat($('#timer').html()));	
	}
	stopTimer();

	//initialize
	var arrayInputRight   	= [];
	var arrayInputLeft 		= [];
	var arrayInputResult 	= [];

	// masukin level ke array
	arrayLevelFinal.push(level);

	// ================================================================================================================================

	//ambil nilai masing masing input left dan masukan ke array
	$("input[data-column='left-column-"+column+"']").each(function(){
	    arrayInputLeft.push(parseInt($(this).val()));
	});
	console.log("LEFT");
	console.log(JSON.stringify(arrayInputLeft));

	arrayLeftInputFinal.push(arrayInputLeft);
	// ================================================================================================================================

	//ambil nilai masing masing input right dan masukan ke array
	$("input[data-column='right-column-"+column+"']").each(function(){
		if($(this).val() != "-"){
			arrayInputRight.push(parseInt($(this).val()));
		}else{
			arrayInputRight.push('-');
		}
	});
	console.log("RIGHT");
	console.log(JSON.stringify(arrayInputRight));

	//masukan array hasil perhitungan user
	arrayRightInputFinal.push(arrayInputRight);

	// ================================================================================================================================
	
	//hitung hasil dari input left, apabila hasilnya 2 digit, ambil digit paling belakang kemudian masukan ke array
	for(var i = 0 ; i< arrayInputLeft.length ; i++){
		if(i == 0) continue;
		var result = arrayInputLeft[i] + arrayInputLeft[i-1];
		var toText = result.toString(); //convert to string
		var lastChar = toText.slice(-1); //gets last character
		arrayInputResult.push(parseInt(lastChar));
	}
	console.log("RESULT");
	console.log(JSON.stringify(arrayInputResult));

	//masukan array hasil perhitungan sistem
	arrayResultFinal.push(arrayInputResult);

	// ================================================================================================================================

	//check array hasil perhitungan sistem dan array hasil input, hasilnya sama apa ngga 
	var correct = true;
	var numberTrue = 0; var numberFalse =0;
	for(var i = 0 ; i< arrayInputRight.length ; i++){
		if(arrayInputRight[i] != arrayInputResult[i]){
			numberFalse++;
		}else{
			numberTrue++;
		}
	}

	if(numberTrue >= 8){
		correct = true;
	}else {
		correct = false;
	}

	// ================================================================================================================================
	
	//dimasukan ke array result per column
	arrayResultStatus.push(correct);

	console.log("correct : " + correct);
	$('.span-result-'+column).html(correct.toString());
	
	// ================================================================================================================================	

	if(column+1 < 3){
		level = 0;
	}else if (column+1 == 4) {
		level = 5;
	}else{
		//kalau ada hasil yang salah dan level lebih dari 0, maka level dikurangi 1
		console.log("level lama : " + level);
		if(correct==false && level != 0 && column > 3){
			level--;
			console.log("level dikurangi 1 : " + level);
		}

		//kalau kolom >= 4 semua
		if(correct==true && column>= 4 && level < 10) {
			level++;
			console.log("level ditambah 1 : " + level);
		}
		console.log("level sekarang : " + level);	
	}

	// ================================================================================================================================

	//find id left input 1 & 2 in the next column
	var leftInputNextColumn = [];
	$("input[data-column='left-column-"+(currentColumn+1)+"']").each(function(){
	    var leftID =parseInt($(this).attr('id').split("-")[1]);
    	leftInputNextColumn.push(leftID);
	});

	//generate new number
	generateRandomNumberPerColumn(column+1);
	var randomNumberPerColumn = allGenerateData[column];

	//set generated number to left input next column
	$('#left-'+leftInputNextColumn[0]).val(randomNumberPerColumn[0]);	
	$('#left-'+leftInputNextColumn[1]).val(randomNumberPerColumn[1]);

	//set timer
	resetTimer();
}

function columnTimeout(){
	//perulangan sebanyak input di kolom saat ini
    var randomNumberPerColumn = allGenerateData[currentColumn-1];
    var i = 0;
    $("input[data-column='left-column-"+currentColumn+"']").each(function(){
		//set generated number to left input next column
		$(this).val(randomNumberPerColumn[i]);
		i++	;
	});

    $("input[data-column='right-column-"+currentColumn+"']").each(function(){
	    if($(this).val() ==""){
	    	//isi nilai -
	    	$(this).val('-')
	    	
	    	//set readonly setelah user input
			$(this).attr('readonly', 'readonly');
	    }

	    //ambil data, untuk cek input halaman terakhir / bukan dan 
	    var lastInput = $(this).attr('data-last-input');
	    var rightID = parseInt($(this).attr('id').split("-")[1]);

	    // if input terakhit dari 1 kolom
    	if(rightID % 10 == 0  ){
    		// Setelah 1 lajur diisi, maka cek hasil input , bener atau ngga
    		checkInputUserPerColumn(currentColumn);

    		//kalau input merupakan input terakhir dari 1 halaman, maka
    		if(lastInput=="true"){
    			if(rightID == 360){
    				//kalau sudah input paling terakhir maka munculin alert / pesan
		    		finishPartTwo();
		    		stopTimer();
		    	}else{
		    		//kalau sudah input terakhir pada sebuah halaman, maka ganti halaman baru
		    		var page = parseInt($(this).attr("data-page"));
			    	$('.row-page').css('display','none');
			    	$('.page-'+(page+1)).css('display','flex');
	    		}
    		}
    		//set focus ke input selanjutnya
    		$('#right-'+(rightID+1)).focus();
    	}
	});
}

function finishPartTwo(){
	var result = {
		left_input: JSON.stringify(arrayLeftInputFinal) ,
	  	right_input: JSON.stringify(arrayRightInputFinal),
	  	expected_result: JSON.stringify(arrayResultFinal),
	  	status_result: JSON.stringify(arrayResultStatus),
	  	time_elapsed: JSON.stringify(arrayTimeElapsed),
	  	level_result: JSON.stringify(arrayLevelFinal),
	};
	console.log(JSON.stringify(result));

	alert("Terima kasih sudah menyelesaikan tes ke dua. Hasil dapat dilihat di console log");
}