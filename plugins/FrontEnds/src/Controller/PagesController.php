<?php
namespace FrontEnds\Controller;

use FrontEnds\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Collection\Collection;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use MyLibs\MyLibs;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Routing\Route\Route;
use Cake\Network\Exception\SocketException;
use Cake\core\Exception\Exception;
use Cake\Network\Exception\ServiceUnavailableException;

/**
 * Content Controller
 *
 * @property \FrontEnds\Model\Table\BlogsTable $Blogs */
class PagesController extends AppController
{

    protected $main_title = 'Pages';

    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
    }
    
    public function isAuthorized($user){
        return true;
    }

    public function beforeRender(Event $event){
        parent::beforeRender($event);
        $this->viewBuilder()->layout('main');

        if ($this->request->params['action'] == 'register' ) {
            $this->viewBuilder()->layout('register');
        }else if (
                $this->request->params['action'] == 'instructionTrialDigitSpan' 
            ||  $this->request->params['action'] == 'instructionDigitSpan' 

            ||  $this->request->params['action'] == 'instructionTrial3dRotation'
            ||  $this->request->params['action'] == 'instruction3dRotation'

            ||  $this->request->params['action'] == 'instructionTrialLineCombination'
            ||  $this->request->params['action'] == 'instructionLineCombination'

            ||  $this->request->params['action'] == 'instructionTrialPaperFolding'
            ||  $this->request->params['action'] == 'instructionPaperFolding'
            
            ||  $this->request->params['action'] == 'instructionTrialBuildingMemory'
            ||  $this->request->params['action'] == 'instructionBuildingMemory'

            ||  $this->request->params['action'] == 'instructionTrialShapeClassification'
            ||  $this->request->params['action'] == 'instructionShapeClassification'

            ||  $this->request->params['action'] == 'instructionTrialFiguralClassification'
            ||  $this->request->params['action'] == 'instructionFiguralClassification'

            ||  $this->request->params['action'] == 'instructionTrialDeretAngka'
            ||  $this->request->params['action'] == 'instructionDeretAngka'
            ) {
            $this->viewBuilder()->layout('instruction');
        }else if ($this->request->params['action'] == 'stageEnd' ) {
            $this->viewBuilder()->layout('stage_end');
        }else {
            $this->viewBuilder()->layout('main');
        }
    }

    public function index(){}

    public function register(){}

    //DIGIT SPAN =================

    public function instructionTrialDigitSpan(){}

    public function stageTrialDigitSpan(){}

    public function instructionDigitSpan(){}

    public function stageDigitSpan(){}

    //3D Rotation =================

    public function instructionTrial3dRotation(){}

    public function stageTrial3dRotation(){}

    public function instruction3dRotation(){}

    public function stage3dRotation(){}

    //Line Combination =================

    public function instructionTrialLineCombination(){}

    public function stageTrialLineCombination(){}

    public function instructionLineCombination(){}

    public function stageLineCombination(){}

    //Paper Folding =================

    public function instructionTrialPaperFolding(){}

    public function stageTrialPaperFolding(){}

    public function instructionPaperFolding(){}

    public function stagePaperFolding(){}

    //Building Memory =================

    public function instructionTrialBuildingMemory(){}

    public function stageTrialBuildingMemory(){}

    public function instructionBuildingMemory(){}

    public function stageBuildingMemory(){}

    //Shape Classification =================

    public function instructionTrialShapeClassification(){}

    public function stageTrialShapeClassification(){}

    public function instructionShapeClassification(){}

    public function stageShapeClassification(){}

    //Figural Classification =================

    public function instructionTrialFiguralClassification(){}

    public function stageTrialFiguralClassification(){}

    public function instructionFiguralClassification(){}

    public function stageFiguralClassification(){}

    public function instructionTrialDeretAngka(){}

    public function stageTrialDeretAngka(){}

    public function instructionDeretAngka(){}

    public function stageDeretAngka(){}

    // public function trialOne(){
    //     $stage_number = "";
    //     $this->set(compact('stage_number'));
    // }
    
    // public function instructionOne(){

    // }

    // public function stageOne(){
    //     $stage_number = "Tahap 1";
    //     $this->set(compact('stage_number'));
    // }

    // public function instructionTwo(){

    // }

    // public function stageTwo(){
    //     $stage_number = "Tahap 2";
    //     $this->set(compact('stage_number'));
    //     //get timer setting
    //     try {
    //         ini_set("allow_url_fopen", 1);
    //         $url='https://admin-gsat.jacarandaindo.com/index.php/api/time_limitation';

    //         // debug(file_get_contents($url));
    //         if(false == ($data = file_get_contents($url)) ){
    //             throw new Exception("Failed connect internet");
    //         }else{
    //             $data=file_get_contents($url);
    //             $data= json_decode($data,JSON_UNESCAPED_SLASHES);
    //             if($data['status'] == true){
    //                 $setting_timer = $data['data']['limit_max'];
    //                 $this->set(compact('setting_timer'));
    //             }
    //         }
    //     } catch (Exception $e) {
    //         throw new ServiceUnavailableException("Periksa kembali koneksi internet anda.", 1);

    //         $setting_timer = 10;
    //         $this->set(compact('setting_timer'));
    //     }
    // }

    public function stageEnd(){

    }
    
}

