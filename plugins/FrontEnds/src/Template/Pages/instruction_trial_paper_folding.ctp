<div class="container instruction">
    <div class="row" style="height: 100%;">
        <div class="col-md-3 instruction-left">
            <h4>Petunjuk Pengerjaan</h4><br>
            <p>Silahkan anda baca dan perhatikan petunjuk pengerjaan tes ini.</p>
        </div>
        <div class="col-md-9 instruction-right">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="instruction-heading">PETUNJUK PENGERJAAN TES VISUALISASI.</h3>
                    <div class="row instruction-form">
                        <div class="col-md-12">
                            <br>
							Selamat datang di Tes Visualisasi [ PAPER FOLDING ]
                            <br><br>
                            Anda akan diberikan runtutan sebuah kertas yang akan dilipat pada masing-masing kotak.
                            Pada kotak terakhir, lipatan kertas tersebut akan dilubangi diposisi tertentu.
                            <br><br>
                            Tugas Anda adalah membayangkan kertas tersebut dilipat lalu dilubangi
                            dan membukanya kertas kembali untuk menentukan posisi bulatan yang muncul.
                            <br><br>
                            Setiap soal memiliki batas waktu dan Anda tidak dapat kembali ke soal tersebut jika Anda sudah menjawab.
                            <br><br>
                            Silahkan menekan angka 1/2/3/4/5 pada keyboard untuk memilih jawaban
							<br><br>
                            Klik tombol <b>'SPACE'</b> untuk memulai bagian percobaan.
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>