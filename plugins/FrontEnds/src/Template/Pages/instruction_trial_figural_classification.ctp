<div class="container instruction">
    <div class="row" style="height: 100%;">
        <div class="col-md-3 instruction-left">
            <h4>Petunjuk Pengerjaan</h4><br>
            <p>Silahkan anda baca dan perhatikan petunjuk pengerjaan tes ini.</p>
        </div>
        <div class="col-md-9 instruction-right">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="instruction-heading">PETUNJUK PENGERJAAN TES VISUALISASI.</h3>
                    <div class="row instruction-form">
                        <div class="col-md-12">
                            <br>
                            Selamat datang di sub tes kategorisasi [ FIGURAL CLASSIFICATION ]
                            <br><br>
                            Anda akan diberikan sekumpulan objek yang telah dikelompokkan menjadi beberapa kategori.
                            Kemudian akan tampil objek lainnya dan tugas Anda adalah mengelompokkan objek tersebut ke dalam kategori tersbut.
                            <br><br>
                            Silahkan menekan angka 1/2/3/4/5 pada keyboard untuk memilih jawaban
							<br><br>
                            Klik tombol <b>'SPACE'</b> untuk memulai bagian percobaan.
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>