<div class="container instruction">
    <div class="row" style="height: 100%;">
        <div class="col-md-3 instruction-left">
            <h4>Petunjuk Pengerjaan</h4><br>
            <p>Silahkan anda baca dan perhatikan petunjuk pengerjaan tes ini.</p>
        </div>
        <div class="col-md-9 instruction-right">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="instruction-heading">PETUNJUK PENGERJAAN TES MEMORY.</h3>
                    <div class="row instruction-form">
                        <div class="col-md-12">
							<br>Selamat datang di tes memori [ DIGIT SPAN ]
                            <br><br>
                            Pada tes ini, Anda diminta untuk mengingat barisan angka yang akan tampil satu demi satu pada layar Anda.
                            Jika semua soal sudah ditampilkan, maka akan ada instruksi untuk menekan angka pada keyboard sesuai dengan angka yang anda lihat sebelumnya sesuai urutannya.
                            <br><br>
                            Contoh: Jika deret angka yang tampil adalah '9 1 2 7', maka Anda harus menekan '9 1 2 7' secara berurutan pada keyboard Anda.
							<br><br>
                            Setelah ini Anda akan memasuki bagian percobaan untuk membantu Anda memahami tes ini.
							<br>
                            Klik tombol <b>'SPACE'</b> untuk memulai bagian percobaan.
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>