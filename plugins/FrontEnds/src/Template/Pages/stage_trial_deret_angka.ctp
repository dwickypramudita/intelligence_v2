<div id="timer"  style="display:none"></div>
<div id="additionalTime" style="display:none">0</div>

<div class="row" style="height: 100%">

	<div id="div-question" style="width: 100%;margin: auto;">
		<div class="col-lg-12">
			<h1><span id="question-text" style="font-size: 60px;letter-spacing: 5px;"></span></h1>
		</div>

		<div class="col-lg-12" style="margin-top: 50px;">
			<label><span id="instruction-text">Pilihlah jawaban yang paling tepat untuk mengisi titik-titik di atas <br>dengan menekan angka 1/2/3/4/5 pada keyboard Anda untuk memilih jawaban</span></label>
		</div>

		<div class="col-lg-12" style="margin-top: 50px;">
			<div class="row col-lg-12" style="margin: 30px 0;">
			<div class="col-lg-1"></div>

			<div class="col-lg-2">
				<div style="width: 100%;max-width: 100px;height: 100px;border: 1px solid white;margin: auto;align-items: center;display: inline-grid;">
					<label ><span id="span-question-1" style="font-size: 30px;"></span></label>
				</div>
				<br>
				<label style="margin-top: 10px;">1</label>
			</div>
			
			<div class="col-lg-2">
				<div style="width: 100%;max-width: 100px;height: 100px;border: 1px solid white;margin: auto;align-items: center;display: inline-grid;">
					<label ><span id="span-question-2" style="font-size: 30px;"></span></label>
				</div>
				<br>
				<label style="margin-top: 10px;">2</label>
			</div>

			<div class="col-lg-2">
				<div style="width: 100%;max-width: 100px;height: 100px;border: 1px solid white;margin: auto;align-items: center;display: inline-grid;">
					<label ><span id="span-question-3" style="font-size: 30px;"></span></label>
				</div>
				<br>
				<label style="margin-top: 10px;">3</label>
			</div>

			<div class="col-lg-2">
				<div style="width: 100%;max-width: 100px;height: 100px;border: 1px solid white;margin: auto;align-items: center;display: inline-grid;">
					<label ><span id="span-question-4" style="font-size: 30px;"></span></label>
				</div>
				<br>
				<label style="margin-top: 10px;">4</label>
			</div>

			<div class="col-lg-2">
				<div style="width: 100%;max-width: 100px;height: 100px;border: 1px solid white;margin: auto;align-items: center;display: inline-grid;">
					<label ><span id="span-question-5" style="font-size: 30px;"></span></label>
				</div>
				<br>
				<label style="margin-top: 10px;">5</label>
			</div>

			<div class="col-lg-1"></div>
		</div>
		</div>
	</div>

	<div id="div-result" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12" id="div-result-correct" style="display: none;">
			<h4><span id="instruction-text">Jawaban Anda Benar !</span></h4>
			<h4><span id="instruction-text">Tekan Spasi untuk melanjutkan</span></h4>
		</div>

		<div class="col-lg-12" id="div-result-incorrect" style="display: none;">
			<h4><span id="instruction-text" style="color:black"><b>Jawaban Anda Salah !</b></span></h4>
			<h4><span id="instruction-text">Tekan Spasi untuk melanjutkan</span></h4>
		</div>
	</div>
</div>