<div class="container instruction">
    <div class="row" style="height: 100%;">
        <div class="col-md-12 instruction-right">
            <div class="row instruction-form">
                <div class="col-md-12" style="text-align: center;vertical-align: middle;">
                    <h3 class="instruction-heading" style="text-align: center;">PETUNJUK PENGERJAAN TES VISUALISASI [ PAPER FOLDING ]</h3>
                    <br><br>
                    Anda telah menyelesaikan bagian percobaan.
                    <br><br>
                    Jika ada yang ingin Anda tanyakan, silahkan bertanya pada petugas.
                    <br><br>
                    Jika tidak ada pertanyaan dan anda sudah siap, silahkan tekan <b>'SPACE'</b>.
                </div>
            </div>
        </div>
    </div>
</div>