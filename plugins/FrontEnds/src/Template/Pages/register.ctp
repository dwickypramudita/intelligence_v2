<div class="container register">
    <div class="row" style="height: 100%">
        <div class="col-md-3 register-left">
            <div class="logo">
              <img class="img-logo" src="<?php echo $this->Url->build('/').'FrontEnds/img/intelligence_logo.jpeg' ?>" alt=""/>
            </div>
            <h4>Selamat datang di Tes Performa.</h4><br>
            <p>Silahkan isi data terlebih dahulu sebelum memulai tes. Pastikan Anda mengisi data yang valid dan pastikan Anda menggunakan komputer / laptop untuk mengerjakan tes ini.</p>
        </div>
        <div class="col-md-9 register-right">
            <!-- <h3 class="register-heading">Mohon Isi Data Diri Anda Yang Sebenar-benarnya.</h3> -->
            <h4 class="register-heading">MOHON ISI DATA DIRI ANDA YANG SEBENAR-BENARNYA</h4>
            <div class="row register-form">
                <div class="col-md-12">
					<form id="form-register">
						<div class="row">
    						<div class="col-sm-6 form-group">
    							<label>Nama</label>
    							<input type="text" id="form-name" name="name" placeholder="Masukan nama kamu disini..." class="form-control">
    						</div>
    						<div class="col-sm-6 form-group">
    							<label>Email</label>
    							<input type="text" id="form-email" name="email" placeholder="Masukan email kamu disini..." class="form-control">
    						</div>
	                    </div>
	                    <div class="row">
    						<div class="col-sm-6 form-group">
    							<label>Telepon</label>
    							<input type="text" id="form-phone-number" name="phone_number" placeholder="Masukan nomor telepon kamu disini..." class="form-control">
    						</div>
    						<div class="col-sm-6 form-group">
    							<label>Jenis Kelamin</label>
    							<div class="form-group form-control" style="background: transparent;border: none;padding-left: 0px;margin-bottom: 0px;">
                                    <label class="radio inline"> 
                                        <input type="radio" class="form-gender" name="gender" value="pria" checked>
                                        <span> Pria </span> 
                                    </label>
                                    <label class="radio inline"> 
                                        <input type="radio" class="form-gender" name="gender" value="wanita">
                                        <span> Wanita </span> 
                                    </label>
                                </div>
    						</div>
	                    </div>
    					<div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Usia </label>
                                <input type="number" placeholder="" class="form-control" id="form-age" min="1" max="100">
                            </div>
    						<div class="col-sm-6 form-group">
	    						<label>Alamat</label>
	    						<input type="text" id="form-address" name="address" placeholder="Masukan alamat kamu disini..." class="form-control">
    						</div>
    					</div>	                    
						<div class="row">
                            <div class="col-sm-6 form-group">
                                <label>Jenjang pendidikan terakhir</label>
                                <select class="form-control" id="form-education">
                                    <option value="SD">SD</option>
                                    <option value="SMP / Sederajat">SMP / Sederajat</option>
                                    <option value="SMA / Sederajat">SMA / Sederajat</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                </select>
                            </div>
    						<div class="col-sm-6 form-group">
                                <label>Institusi yang akan dilamar</label>
                                <input type="text" placeholder="" class="form-control" id="form-institution">
                            </div>
	                    </div>
						<div class="row" style="margin-top: 10px;">
							<div class="col-lg-6">
								<div class="center-holder">

									<div id="message-warning" style="width: auto;text-align: left;margin: 5px 0px;color: red;display: none;">              
			                        </div>            
			                        
			                        <!-- contact-success -->
			                        <div id="message-success" style="width: auto;text-align: left;margin: 5px 0px;color:green;display: none;" >              
			                             
			                        </div>
			                        
			                        <div id="submit-loader" class="submit-loader" style="margin: 5px 10px;display: none;">
			                            <div class="text-loader" style="color: blue;">Mengirimkan data peserta...</div>                             
			                            <div class="s-loader">
			                              <div class="bounce1"></div>
			                              <div class="bounce2"></div>
			                              <div class="bounce3"></div>
			                            </div>
			                        </div>

								</div>						
							</div>
							<div class="col-lg-6">
								<input type="submit" class="btnRegister"  value="Kirim Data Peserta"/>
								<!-- <a class="btn btnNewTest" href="<?php echo $this->Url->build('/'); ?>">Mulai Tes Baru</a> -->
							</div>
						</div>

					</form> 
				</div>
            </div>
        </div>
    </div>

</div>