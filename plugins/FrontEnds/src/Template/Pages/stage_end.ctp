<div class="container instruction">
    <div class="row" style="height: 100%;">
        <div class="col-md-12 instruction-right">
            <div class="row instruction-form">
                <div class="col-md-12 col-xs-12" style="text-align: center;vertical-align: middle;">
                	<h3 class="instruction-heading" style="text-align: center;">Semua Tahap Tes Selesai</h3>
                	<br>
					<br>

					Terima kasih sudah menyelesaikan seluruh rangkaian tes performa. Jangan tutup Halaman ini apabila proses pengiriman data sedang dilakukan.
					<br>
					<br>

					<div id="message-warning" style="width: auto;margin: 5px 0px;color: red;display: none;"></div>            
                    <div id="message-success" style="width: auto;margin: 5px 0px;color:green;display: none;"></div>
                    <div id="submit-loader" class="submit-loader" style="margin: 5px 10px;display: none;">
                        <div class="text-loader" style="color: blue;">Proses Mengirimkan data tes...</div>                             
                        <div class="s-loader">
                          <div class="bounce1"></div>
                          <div class="bounce2"></div>
                          <div class="bounce3"></div>
                        </div>
                    </div>

                    <br>
                    <br>

                    <button class="btn btnResendData btn-primary" style="display: none;">Kirim Ulang Data Test</button>
                    <a class="btn btnNewTest btn-primary" href="<?php echo $this->Url->build('/'); ?>" style="display: none;">Mulai Tes Baru</a>

				</div>
            </div>
        </div>
    </div>
</div>