<div id="timer"  style="display:none"></div>
<div id="additionalTime" style="display:none">0</div>

<div class="row" style="height: 100%">

	<div id="div-question" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12" >
			<h4><span id="instruction-text-1">Kelompokkan gambar di bawah ke dalam grup 1 / grup 2</span></h4>
			<h4><span id="instruction-text-2"><b>TEKAN ANGKA 1 / 2 UNTUK MEMILIH JAWABAN</b></span></h4>
		</div>
		<div class="col-lg-12" style="margin-top: 30px;">
			<img class="img-logo" id="img-question" src="" alt="" style="width: 910px;height: 200px;"/>
		</div>
	
		<div class="row col-lg-12" style="margin: 30px 0;">
			<div class="col-lg-5"></div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-1" src="" alt="" style="width: 200px;height: 200px;" >
			</div>
			
			<div class="col-lg-5"></div>
		</div>
	</div>

	<div id="div-result" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12" id="div-result-correct" style="display: none;">
			<h4><span id="instruction-text">Jawaban Anda Benar !</span></h4>
			<h4><span id="instruction-text">Tekan Spasi untuk melanjutkan</span></h4>
		</div>

		<div class="col-lg-12" id="div-result-incorrect" style="display: block;">
			<h4><span id="instruction-text" style="color:black"><b>Jawaban Anda Salah !</b></span></h4>
			<h4><span id="instruction-text">Jawaban yang benar adalah yang diberi lingkaran merah</span></h4>
			<h4><span id="instruction-text">Tekan Spasi untuk melanjutkan</span></h4>
			<br>
			<div class="col-lg-12">
				<img class="img-logo" id="img-expected" src="" alt="" style="width: 950px;height: 470px;"/>
			</div>
		</div>
	</div>
</div>