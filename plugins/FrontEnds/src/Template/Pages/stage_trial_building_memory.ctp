<div id="timer"  style="display:none"></div>
<div id="additionalTime" style="display:none">0</div>

<div class="row" style="height: 100%">

	<div id="div-memorization" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12">
			<h4><span id="instruction-text" >Silahkan mengingat posisi setiap objek pada gambar berikut ini:</span></h4>
			<br>
			<div class="col-lg-12">
				<img class="img-logo" id="img-memorization" src="" alt="" style="width: 450px;height: 450px;"/>
			</div>
		</div>
	</div>

	<div id="div-delay" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12">
			<h4><span id="instruction-text" >Silahkan menunggu hingga halaman soal ditampilkan</span></h4>
			<br>
			<h4><span id="span-timer-countdown" >5</span></h4>
		</div>
	</div>

	<div id="div-correct" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12">
			<h4><span id="instruction-text" style="color: black">Jawaban Anda Salah !</span></h4>
			<h4><span id="instruction-text">Jawaban yang benar adalah yang diberi lingkaran merah</span></h4>
			<br>
			<div class="col-lg-12">
				<img class="img-logo" id="img-correct" src="" alt="" style="width: 350px;height: 350px;"/>
			</div>
		</div>
	</div>

	<div id="div-question" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12 text-center">
			<h4><span id="instruction-text" >Di manakah letak objek ini sesuai dengan gambar yang Anda lihat sebelumnya?</span></h4>
			<br>
			<div class="col-lg-12">
				<img class="img-logo" id="img-question" src="" alt="" style="width: 150px;height: 150px;"/>
			</div>
			<div class="col-lg-12" style="margin-top: 20px;">
				<img class="img-logo" id="img-question-frame" src="" alt="" style="width: 150px;height: 150px;"/>
			</div>
		</div>

		<div class="row col-lg-12" style="margin: 30px 0;">
			<div class="col-lg-1"></div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-1" src="" alt="" 
				style="width: 100%;max-width:100px;height: 100px;"/><br>
				<label style="margin-top: 10px;">1</label>
			</div>
			
			<div class="col-lg-2">
				<img class="img-logo" id="img-question-2" src="" alt="" 
				style="width: 100%;max-width:100px;height: 100px;"/><br>
				<label style="margin-top: 10px;">2</label>
			</div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-3" src="" alt="" 
				style="width: 100%;max-width:100px;height: 100px;"/><br>
				<label style="margin-top: 10px;">3</label>
			</div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-4" src="" alt="" 
				style="width: 100%;max-width:100px;height: 100px;"/><br>
				<label style="margin-top: 10px;">4</label>
			</div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-5" src="" alt="" 
				style="width: 100%;max-width:100px;height: 100px;"/><br>
				<label style="margin-top: 10px;">5</label>
			</div>

			<div class="col-lg-1"></div>
		</div>

		<div class="col-lg-12 text-center" style="margin-top: 20px;">
			<h4><span id="instruction-text" >Tekan angka yang menurut anda benar dengan menggunakan keyboard</span></h4>
			<br>
			<h2><span id="span-input-user" style="font-size: 30px;"></span></h2>
		</div>
	</div>
	
</div>