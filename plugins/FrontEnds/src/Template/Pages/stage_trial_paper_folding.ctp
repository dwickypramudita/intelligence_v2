<div id="timer"  style="display:none"></div>
<div id="additionalTime" style="display:none">0</div>

<div class="row" style="height: 100%">

	<div id="div-question" style="width: 100%;display: none;margin: auto;">
		<div class="row col-lg-12" style="margin-top: 30px;">
			<div class="col-lg-12">
				<img class="img-logo" id="img-question" src="" alt="" style="width: 650px;height: 200px;"/>
			</div>
		</div>

		<div class="row col-lg-12" style="margin-top: 30px;">
			<div class="col-lg-1"></div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-1" src="" alt="" 
				style="width: 100%;max-width:150px;height: 150px;"/><br>
				<label style="margin-top: 10px;">1</label>
			</div>
			
			<div class="col-lg-2">
				<img class="img-logo" id="img-question-2" src="" alt="" 
				style="width: 100%;max-width:150px;height: 150px;"/><br>
				<label style="margin-top: 10px;">2</label>
			</div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-3" src="" alt="" 
				style="width: 100%;max-width:150px;height: 150px;"/><br>
				<label style="margin-top: 10px;">3</label>
			</div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-4" src="" alt="" 
				style="width: 100%;max-width:150px;height: 150px;"/><br>
				<label style="margin-top: 10px;">4</label>
			</div>

			<div class="col-lg-2">
				<img class="img-logo" id="img-question-5" src="" alt="" 
				style="width: 100%;max-width:150px;height: 150px;"/><br>
				<label style="margin-top: 10px;">5</label>
			</div>

			<div class="col-lg-1"></div>
		</div>

		<div class="col-lg-12 text-center" style="margin-top: 20px;">
			<h4><span id="instruction-text" >Tekan angka yang menurut anda benar dengan menggunakan keyboard</span></h4>
			<br>
			<h2><span id="span-input-user" style="font-size: 30px;"></span></h2>
		</div>
	</div>

	<div id="div-result" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12" id="div-result-correct" style="display: none;">
			<h4><span id="instruction-text">Jawaban Anda Benar !</span></h4>
			<h4><span id="instruction-text">Tekan Spasi untuk melanjutkan</span></h4>
		</div>

		<div class="col-lg-12" id="div-result-incorrect" style="display: block;">
			<h4><span id="instruction-text" style="color:black"><b>Jawaban Anda Salah !</b></span></h4>
			<h4><span id="instruction-text">Jawaban yang benar adalah yang diberi lingkaran merah</span></h4>
			<h4><span id="instruction-text">Tekan Spasi untuk melanjutkan</span></h4>
			<br>
			<div class="col-lg-12">
				<img class="img-logo" id="img-expected" src="" alt="" style="width: 950px;height: 470px;"/>
			</div>
		</div>
	</div>
	
</div>