<div id="timer"  style="display:none"></div>
<div id="additionalTime" style="display:none">0</div>

<div class="row" style="height: 100%">
	
	<div id="div-question" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12 text-center" style="margin-top: 100px;">
			<h1 style="font-size: 70px;"><span id="span-test-question"></span></h1>
		</div>

		<div class="col-lg-12 text-center" style="margin-top: 100px;">
			<h4><span id="instruction-text" style="display: none;">Tekan angka yang Anda ingat secara berurutan dengan menggunakan keyboard</span></h4>

			<br>
			<br>

			<h2><span id="span-input-user" style="font-size: 50px;"></span></h2>
		</div>
	</div>

	<div id="div-delay" style="width: 100%;display: none;margin: auto;">
		<div class="col-lg-12">
			<h4><span id="instruction-text" >Silahkan menunggu hingga halaman soal ditampilkan</span></h4>
			<br>
			<h4><span id="span-timer-countdown" >5</span></h4>
		</div>
	</div>

</div>