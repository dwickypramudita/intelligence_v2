<div class="container instruction">
    <div class="row" style="height: 100%;">
        <div class="col-md-3 instruction-left">
            <h4>Petunjuk Pengerjaan</h4><br>
            <p>Silahkan anda baca dan perhatikan petunjuk pengerjaan tes ini.</p>
        </div>
        <div class="col-md-9 instruction-right">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="instruction-heading">PETUNJUK PENGERJAAN TES VISUALISASI.</h3>
                    <div class="row instruction-form">
                        <div class="col-md-12">
                            <br>
							Selamat datang di subtes Objek [ BUILDING MEMORY ]
                            <br><br>
                            Pada tes ini, layar Anda akan menampilkan suatu gambar yang berisi beberapa objek.
                            Anda diminta untuk mengingat letak setiap objek tersebut.
                            <br><br>
                            Kemudian gambar tersebut akan hilang dan Anda akan diminta
                            untuk menjawab beberapa pertanyaan berdasarkan objek yang ada dalam gambar tadi.
                            <br><br>
                            Tugas Anda adalah memilih dimana letak objek tersebut sesuai dengan gambar yang Anda lihat sebelumnya.
                            <br><br>
                            Perlu Anda ketahui bahwa 1 gambar yang akan Anda ingat posisinya,
                            akan digunakan untuk menjawab tiga (3) sampai empat (4) soal berikutnya
                            <br><br>
                            Silahkan menekan angka 1/2/3/4/5 pada keyboard untuk memilih jawaban.
							<br><br>
                            Klik tombol <b>'SPACE'</b> untuk memulai bagian percobaan.
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>