// CHECK IF ALL STAGE COMPLETE
var VALUE_REGISTRATION 		= localStorage.getItem("value_registration");
var VALUE_DIGIT_SPAN 		= localStorage.getItem("value_stage_digit_span");
var VALUE_3D_ROTATION 		= localStorage.getItem("value_stage_3d_rotation");
var VALUE_LINE_COMBINATION  = localStorage.getItem("value_stage_line_combination");
var VALUE_PAPER_FOLDING 	= localStorage.getItem("value_stage_paper_folding");
var VALUE_BUILDING_MEMORY 	= localStorage.getItem("value_stage_building_memory");
var VALUE_SHAPE_CLASSIFICATION 	 = localStorage.getItem("value_stage_shape_classification");
var VALUE_FIGURAL_CLASSIFICATION = localStorage.getItem("value_stage_figural_classification");
var VALUE_DERET_ANGKA 		= localStorage.getItem("value_stage_deret_angka");


VALUE_REGISTRATION 		= JSON.parse(VALUE_REGISTRATION);
VALUE_DIGIT_SPAN 		= JSON.parse(VALUE_DIGIT_SPAN);
VALUE_3D_ROTATION 		= JSON.parse(VALUE_3D_ROTATION);
VALUE_LINE_COMBINATION  = JSON.parse(VALUE_LINE_COMBINATION);
VALUE_PAPER_FOLDING 	= JSON.parse(VALUE_PAPER_FOLDING);
VALUE_BUILDING_MEMORY 	= JSON.parse(VALUE_BUILDING_MEMORY);
VALUE_SHAPE_CLASSIFICATION 	 = JSON.parse(VALUE_SHAPE_CLASSIFICATION);
VALUE_FIGURAL_CLASSIFICATION = JSON.parse(VALUE_FIGURAL_CLASSIFICATION);
VALUE_DERET_ANGKA 		= JSON.parse(VALUE_DERET_ANGKA);

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else if(VALUE_STAGE_1==null ){
// 	alert("Anda harus menyelesaikan tahap 1 terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_one'); ?>");
// }else if(VALUE_STAGE_2 == null){
// 	alert("Anda harus menyelesaikan tahap 2 terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_two'); ?>");
// }else{
// 	user_registration = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_STAGE_1 = JSON.parse(localStorage.getItem("value_stage_1"));
// 	VALUE_STAGE_2 = JSON.parse(localStorage.getItem("value_stage_2"));
// }

// var VALUE_DATA = $.merge(VALUE_STAGE_1, VALUE_STAGE_2 );

var raw_data = {
    'user_identity' : VALUE_REGISTRATION,
    'digit_span' : VALUE_DIGIT_SPAN,
    '3D' : VALUE_3D_ROTATION,
    'line_combination' : VALUE_LINE_COMBINATION,
    'building_memory' : VALUE_BUILDING_MEMORY,
    'paper_folding' : VALUE_PAPER_FOLDING,
    'shape_classification' : VALUE_SHAPE_CLASSIFICATION,
    'deret_angka' : VALUE_DERET_ANGKA,
    'figural_classification' : VALUE_FIGURAL_CLASSIFICATION
}

console.log(raw_data);

raw_data = JSON.stringify(raw_data);

console.log(raw_data);

// var flag_blink = false;
// function blink(selector){
//   if(flag_blink){
//     $(selector).animate({opacity:0}, 50, "linear", function(){
//         $(this).delay(800);
//         $(this).animate({opacity:1}, 50, function(){
//           blink(this);
//         });
//         $(this).delay(800);
//     });
//   } 
// }

// $('#submit-loader').show();
// $('#message-warning').hide();
// $('#message-success').hide(); 

// var sLoader = $('#submit-loader');

// function sendData(){
// 	$.ajax({        
// 		type: "post",
// 	    url: 'https://admin-gsat.jacarandaindo.com/index.php/api/result',
// 	    cache: false,
// 	    contentType: "application/json",
// 	    dataType : "json",
// 	    data : raw_data,
// 		beforeSend: function() { 
// 		  sLoader.fadeIn(); 
// 		  sLoader.show();
// 		  flag_blink = true;
// 		  blink(sLoader);
// 		  $(".btnResendData").attr("disabled", 'disabled');
// 		},
// 		success:function(data) {
		  
// 		  if (data.status== true) {
// 		      sLoader.fadeOut(); 
// 		      sLoader.hide();
// 		      flag_blink = false;
// 		      $('#message-warning').html("");
// 		      $('#message-warning').hide();
		        
// 		      $('#message-success').html('<i class="fa fa-check"></i>Data anda telah berhasil dikirim. Silakan tekan tombol "Mulai Tes Baru" untuk memulai tes kembali');
// 		      $('#message-success').fadeIn();

// 		      $(".btnResendData").attr("disabled", false); 
// 		      $(".btnResendData").css("display", 'none');

// 		      $(".btnNewTest").css("display", 'inline-block'); 

// 		      window.localStorage.removeItem("value_registration");
// 		      window.localStorage.removeItem("value_trial_1");
// 		      window.localStorage.removeItem("value_stage_1");
// 		      window.localStorage.removeItem("value_stage_2");
// 			  window.localStorage.clear();
// 		  }
// 		  // There was an error
// 		  else {
// 		      sLoader.fadeOut(); 
// 		      sLoader.hide();
// 		      flag_blink = false;
// 		      $('#message-warning').html(data.message);
// 		      $('#message-warning').fadeIn();

// 		      $('#message-success').html("");

// 		      $(".btnResendData").attr("disabled", false);
// 		      $(".btnResendData").css("display", "inline-block");
// 		      $(".btnNewTest").css("display", 'none'); 
// 		  }
// 		},
// 		error: function() {
// 		  sLoader.fadeOut(); 
// 		  sLoader.hide();
// 		  flag_blink = false;
// 		  $('#message-warning').html("Terjadi kesalahan, periksa koneksi internet anda atau silahkan coba lagi.");
// 		  $('#message-warning').fadeIn();
// 		  $(".btnResendData").attr("disabled", false);
// 	      $(".btnResendData").css("display", "inline-block");
// 	      $(".btnNewTest").css("display", 'none'); 
// 		}

// 	});
// }

// $( document ).ready(function() {
// 	sendData();
// 	$('body').on('click','.btnResendData',function(e){
// 		sendData();
// 	});
// });