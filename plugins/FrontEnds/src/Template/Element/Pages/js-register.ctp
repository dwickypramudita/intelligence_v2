var flag_blink = false;
function blink(selector){
  if(flag_blink){
    $(selector).animate({opacity:0}, 50, "linear", function(){
        $(this).delay(800);
        $(this).animate({opacity:1}, 50, function(){
          blink(this);
        });
        $(this).delay(800);
    });
  } 
}

function validatePhone(txtPhone) {
    var filter = /^[0-9-+]+$/;
    if (filter.test(txtPhone)) {
        return true;
    }
    else {
        return false;
    }
}

$( document ).ready(function() {
    
    window.localStorage.clear();
    
    $('body').on('click','.btnRegister',function(e){
    	e.preventDefault();

	    //validation  --------------------------------------------------------------------

    	$(".error").hide();
        var hasError = false;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 
        var emailaddressVal = $("#form-email").val();
        if(emailaddressVal == '') {
            $("#form-email").after('<small class="error">Mohon isi alamat email anda.</small>');
            hasError = true;
        }
 
        else if(!emailReg.test(emailaddressVal)) {
            $("#form-email").after('<small class="error">Mohon isi alamat email yang valid.</small>');
            hasError = true;
        }
 		
        if($("#form-name").val() == '') {
            $("#form-name").after('<small class="error">Mohon isi nama anda.</small>');
            hasError = true;
        }

        if($("#form-phone-number").val() == '') {
            $("#form-phone-number").after('<small class="error">Mohon isi nomor telepon anda.</small>');
            hasError = true;
        }else if (!validatePhone($("#form-phone-number").val())) {
        	$("#form-phone-number").after('<small class="error">Mohon isi nomor telepon anda dengan benar.</small>');
            hasError = true;
        }

        if($("#form-address").val() == '') {
            $("#form-address").after('<small class="error">Mohon isi alamat anda.</small>');
            hasError = true;
        }

        if($("#form-age").val() == '') {
            $("#form-age").after('<small class="error">Mohon isi umur anda.</small>');
            hasError = true;
        }

        // if($("#form-institution").val() == '') {
        //     $("#form-institution").after('<small class="error">Mohon isi institusi yang akan anda lamar.</small>');
        //     hasError = true;
        // }

        if($("#form-education").val() == '') {
            $("#form-education").after('<small class="error">Mohon isi jenjang pendidikan terakhir anda.</small>');
            hasError = true;
        }

        //----------------------------------------------------------------------------------------

	    var sLoader = $('#submit-loader');
	    
        if(hasError == true) {
		  $('#message-warning').html("Error : Mohon cek kembali data anda.");
		  sLoader.fadeOut(); 
		  $('#message-warning').fadeIn();
		  return;
		}

		$('#submit-loader').show();
		$('#message-warning').hide();
		$('#message-success').hide(); 

		//----------------------------------------------------------------------------------------

		var user_registration = {
    		name : $('#form-name').val(),
    		email : $('#form-email').val(),
    		phone_number : $('#form-phone-number').val(),
    		address : $('#form-address').val(),
    		gender : $('input[name="gender"]:checked').val(),
    		age : $('#form-age').val(),
    		education : $('#form-education').val(),
    		institution : $('#form-institution').val(),
    	};
	    	
	    var raw_data = JSON.stringify(user_registration);

		sLoader.fadeIn(); 
		sLoader.show();
		flag_blink = true;
		blink(sLoader);

		setTimeout(function(){
		    window.localStorage.setItem('value_registration', raw_data);
			window.location.replace("<?php echo $this->Url->build('/instruction_trial_digit_span'); ?>");
	  	}, 4000);

	});
});
