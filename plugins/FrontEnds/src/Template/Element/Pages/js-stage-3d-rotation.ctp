// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

var URL_IMAGE_PATH = "<?php echo $this->Url->build('/').'FrontEnds/img/3d_rotation/' ?>";

// intialize data
var question_trial = [{"path":URL_IMAGE_PATH+"trial/trial_1a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1d.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_1e.png","answer":false}]

//DONE
var ARRAY_QUESTION = [
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":true}]
	},
	{
	 "level" : "easy",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"easy/easy_2.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_2a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_2b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_2c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_2d.png","answer":true},{"path":URL_IMAGE_PATH+"easy/easy_2e.png","answer":false}]
	},
	{
	 "level" : "easy",
	 "sequence" : "3",
	 "path" : URL_IMAGE_PATH+"easy/easy_3.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_3a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_3b.png","answer":true},{"path":URL_IMAGE_PATH+"easy/easy_3c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_3d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_3e.png","answer":false}]
	},
	{
	 "level" : "easy",
	 "sequence" : "4",
	 "path" : URL_IMAGE_PATH+"easy/easy_4.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_4a.png","answer":true},{"path":URL_IMAGE_PATH+"easy/easy_4b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_4c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_4d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_4e.png","answer":false}]
	},
	{
	 "level" : "easy",
	 "sequence" : "5",
	 "path" : URL_IMAGE_PATH+"easy/easy_5.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"easy/easy_5a.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_5b.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_5c.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_5d.png","answer":false},{"path":URL_IMAGE_PATH+"easy/easy_5e.png","answer":true}]
	},





	{
	 "level" : "medium",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"medium/medium_2.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_2a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_2b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_2c.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_2d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_2e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "3",
	 "path" : URL_IMAGE_PATH+"medium/medium_3.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_3a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_3b.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_3c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_3d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_3e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "4",
	 "path" : URL_IMAGE_PATH+"medium/medium_4.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_4a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_4b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_4c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_4d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_4e.png","answer":true}]
	},
	{
	 "level" : "medium",
	 "sequence" : "5",
	 "path" : URL_IMAGE_PATH+"medium/medium_5.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_5a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_5b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_5c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_5d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_5e.png","answer":true}]
	},
	{
	 "level" : "medium",
	 "sequence" : "6",
	 "path" : URL_IMAGE_PATH+"medium/medium_6.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_6a.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_6b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_6c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_6d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_6e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "7",
	 "path" : URL_IMAGE_PATH+"medium/medium_7.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_7a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_7b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_7c.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_7d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_7e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "8",
	 "path" : URL_IMAGE_PATH+"medium/medium_8.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_8a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_8b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_8c.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_8d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_8e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "9",
	 "path" : URL_IMAGE_PATH+"medium/medium_9.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_9a.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_9b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_9c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_9d.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_9e.png","answer":false}]
	},
	{
	 "level" : "medium",
	 "sequence" : "10",
	 "path" : URL_IMAGE_PATH+"medium/medium_10.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"medium/medium_10a.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_10b.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_10c.png","answer":false},{"path":URL_IMAGE_PATH+"medium/medium_10d.png","answer":true},{"path":URL_IMAGE_PATH+"medium/medium_10e.png","answer":false}]
	},




	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":false}]
	},
	{
	 "level" : "hard",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"hard/hard_2.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_2a.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_2b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_2c.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_2d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_2e.png","answer":false}]
	},
	{
	 "level" : "hard",
	 "sequence" : "3",
	 "path" : URL_IMAGE_PATH+"hard/hard_3.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_3a.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_3b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_3c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_3d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_3e.png","answer":true}]
	},
	{
	 "level" : "hard",
	 "sequence" : "4",
	 "path" : URL_IMAGE_PATH+"hard/hard_4.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_4a.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_4b.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_4c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_4d.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_4e.png","answer":false}]
	},
	{
	 "level" : "hard",
	 "sequence" : "5",
	 "path" : URL_IMAGE_PATH+"hard/hard_5.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"hard/hard_5a.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_5b.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_5c.png","answer":false},{"path":URL_IMAGE_PATH+"hard/hard_5d.png","answer":true},{"path":URL_IMAGE_PATH+"hard/hard_5e.png","answer":false}]
	},
];

var ARRAY_ALL_TEST_RESULT 	= [];
var TOTAL_QUESTION			= ARRAY_QUESTION.length;
var CURRENT_SEQ_QUESTION 	= 0;
var CURRENT_LEVEL 			= "";
var TIMELIMIT 				= 10000;
var CURRENT_QUESTION 		= {};

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

$( document ).ready(function() {
    newPage();
});

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_3d_rotation");

	//initialize
	GenerateAndshowQuestion();
}

function GenerateAndshowQuestion(){

	//tentukan jumlah berapa digit
	if(CURRENT_SEQ_QUESTION < TOTAL_QUESTION){
		
		//initialize
		CURRENT_QUESTION = ARRAY_QUESTION[CURRENT_SEQ_QUESTION];
		CURRENT_LEVEL 		 = CURRENT_QUESTION.level;
		USER_ANSWER 		 = "";
		EXPECTED_ANSWER 	 = "";

		//set user input empty
		$('#span-input-user').text("");

		//set img src soal
		$("#img-question").attr("src",CURRENT_QUESTION.path);

		//get array question
		ARRAY_CHOICE_CURRENT_QUESTION = CURRENT_QUESTION.choice;
		ARRAY_CHOICE_CURRENT_QUESTION=_.shuffle(ARRAY_CHOICE_CURRENT_QUESTION); 

		//show image
		for(var i = 0 ; i < ARRAY_CHOICE_CURRENT_QUESTION.length ; i++){

			var temp = ARRAY_CHOICE_CURRENT_QUESTION[i];

			//tampilkan ke img
			$("#img-question-"+(i+1)).attr("src",temp.path);

			//ambil jawaban yang benar
			if(temp.answer == true){
				EXPECTED_ANSWER = (i+1);
			}
		}

		//set timer
		initializeTimer();

		//tambahkan event listener
		window.addEventListener("keypress", checkKeyPressed, false);

		//check keypress
		function checkKeyPressed(e) {
			//check inputan user
			if (e.charCode >=  49 && e.charCode <= 53  )  {
				switch(e.charCode){
					case 49 : USER_ANSWER = "1"; break;
					case 50 : USER_ANSWER = "2"; break;
					case 51 : USER_ANSWER = "3"; break;
					case 52 : USER_ANSWER = "4"; break;
					case 53 : USER_ANSWER = "5"; break;
				}

				//tampilkan hasil inputan
				$('#span-input-user').text(USER_ANSWER);

				// hentikan waktu
				clearTimeout(timeout);

				//cek jawaban
				checkInput(USER_ANSWER,EXPECTED_ANSWER);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressed, false);
			}
		}

		//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
		var timeout = setTimeout(function(){
			//cek jawaban
			checkInput(USER_ANSWER,EXPECTED_ANSWER);

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressed, false);
		},TIMELIMIT);
	}	
}

function checkInput(USER_ANSWER,EXPECTED_ANSWER){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = false;
	if(USER_ANSWER == EXPECTED_ANSWER){
		correct = true;
		
	}else{
		correct = false;
	}

	var resultQuestion = {
		stage : "3d_rotation",
		level : CURRENT_LEVEL,
		number : CURRENT_QUESTION.sequence,
		time_elapsed : time_elapsed,
		status_result : correct,
		expected : EXPECTED_ANSWER,
		answer : USER_ANSWER
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	var timeout = setTimeout(function(){
		//generate next question
		CURRENT_SEQ_QUESTION++;

		if(CURRENT_SEQ_QUESTION < TOTAL_QUESTION){
			GenerateAndshowQuestion();
		}else{
			finishTest();
		}
	},1000);
}

function finishTest(){
	window.localStorage.setItem('value_stage_3d_rotation', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan tes 3D rotation. Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_trial_line_combination'); ?>");
}