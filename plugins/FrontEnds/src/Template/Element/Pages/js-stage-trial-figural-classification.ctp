// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

var URL_IMAGE_PATH = "<?php echo $this->Url->build('/').'FrontEnds/img/figural_classification/' ?>";

// intialize data
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"trial/trial_1a.png","expected" : URL_IMAGE_PATH+"trial/trial_1a_expected.png","answer":2},
			{"path":URL_IMAGE_PATH+"trial/trial_1b.png","expected" : URL_IMAGE_PATH+"trial/trial_1b_expected.png","answer":1}
		]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"trial/trial_1a.png","expected" : URL_IMAGE_PATH+"trial/trial_1a_expected.png","answer":2},
			{"path":URL_IMAGE_PATH+"trial/trial_1b.png","expected" : URL_IMAGE_PATH+"trial/trial_1b_expected.png","answer":1}
		]
	}
];

var ARRAY_ALL_TEST_RESULT 	= [];

var TOTAL_MAIN_QUESTION		= ARRAY_QUESTION.length;
var CURRENT_SEQ_MAIN_QUESTION 	= 0;
var CURRENT_MAIN_QUESTION 	= {};

var ARRAY_CURRENT_SUB_QUESTION 	= [];
var TOTAL_SUB_QUESTION			= 0;
var CURRENT_SEQ_SUB_QUESTION 	= 0;
var CURRENT_SUB_QUESTION 		= {};

var CURRENT_LEVEL 			= "";
var TIMELIMIT_ANSWER 		= 10000;
var TIMELIMIT_MEMORIZATION 	= 10000;
var COUNTER_PREPARATION		= 5

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

function hideAllDiv(){
	$('#div-question').css("display","none");
	$('#div-result').css("display","none");
}

$( document ).ready(function() {
    newPage();
});

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_trial_figural_classification");

	//initialize
	GenerateAndshowMainQuestion();
}

function GenerateAndshowMainQuestion(){
	//tentukan jumlah berapa digit
	if(CURRENT_SEQ_MAIN_QUESTION < TOTAL_MAIN_QUESTION){
		
		//set main
		CURRENT_MAIN_QUESTION 		= ARRAY_QUESTION[CURRENT_SEQ_MAIN_QUESTION];
		CURRENT_LEVEL 		 		= CURRENT_MAIN_QUESTION.level;

		//set sub
		ARRAY_CURRENT_SUB_QUESTION 	= CURRENT_MAIN_QUESTION.question;
		TOTAL_SUB_QUESTION 			= ARRAY_CURRENT_SUB_QUESTION.length;
		CURRENT_SEQ_SUB_QUESTION 	= 0;
		
		//sembunyikan semua div
		hideAllDiv();

		$('#div-question').css("display","block");
		$("#img-question").attr("src",CURRENT_MAIN_QUESTION.path);

        showQuestion();

	}else{
		finishTest();
	}
}

function showQuestion(){
	if(CURRENT_SEQ_SUB_QUESTION < TOTAL_SUB_QUESTION){

		//sembunyikan semua div
		hideAllDiv();

		//tampilkan div question
        $('#div-question').css("display","block");

		//initialize
		var USER_ANSWER 		 = "";
		var EXPECTED_ANSWER 	 = "";

		//ambil soal berikutnya
		CURRENT_SUB_QUESTION = ARRAY_CURRENT_SUB_QUESTION[CURRENT_SEQ_SUB_QUESTION];

		$("#img-question-1").attr("src",CURRENT_SUB_QUESTION.path);
		EXPECTED_ANSWER = CURRENT_SUB_QUESTION.answer;

		//jalankan waktu stopwatch
		initializeTimer();

		//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
		var timeout = setTimeout(function(){
			// hentikan waktu
			clearTimeout(timeout);

			//cek jawaban
			checkInput(USER_ANSWER,EXPECTED_ANSWER);

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressed, false);
		},TIMELIMIT_ANSWER);

		//tambahkan event listener
		window.addEventListener("keypress", checkKeyPressed, false);

		//check keypress
		function checkKeyPressed(e) {
			//check inputan user
			if (e.charCode >=  49  && e.charCode <= 50  )  {
				switch(e.charCode){
					case 49 : USER_ANSWER = "1"; break;
					case 50 : USER_ANSWER = "2"; break;
				}

				// hentikan waktu
				clearTimeout(timeout);

				//cek jawaban
				checkInput(USER_ANSWER,EXPECTED_ANSWER);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressed, false);
			}
		}
	}else{
		CURRENT_SEQ_MAIN_QUESTION++;
		GenerateAndshowMainQuestion();
	}
}

function checkInput(USER_ANSWER,EXPECTED_ANSWER){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = false;
	if(USER_ANSWER == EXPECTED_ANSWER){
		correct = true;

		hideAllDiv();
		$('#div-result').css("display","block");
		$('#div-result-correct').css("display","block");
		$('#div-result-incorrect').css("display","none");
		
		//ada jeda waktu 10 detik sebelum lanjut soal berikutnya
		var timeout_result = setTimeout(function(){
        	//sembunyikan semua div
			hideAllDiv();
			
			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressedResult, false);

	        insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
		},5000);

		window.addEventListener("keypress", checkKeyPressedResult, false);

		//check keypress
		function checkKeyPressedResult(e) {
			//check inputan user
			if (e.charCode ==  32 )  {
				// hentikan waktu
				clearTimeout(timeout_result);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressedResult, false);

				insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
			}
		}

	}else{
		correct = false;
		
		hideAllDiv();
		$('#div-result').css("display","block");
		$('#div-result-correct').css("display","none");
		$('#div-result-incorrect').css("display","block");
		
		$("#img-expected").attr("src",CURRENT_SUB_QUESTION.expected);

		//ada jeda waktu 10 detik sebelum lanjut soal berikutnya
		var timeout_result = setTimeout(function(){
        	//sembunyikan semua div
			hideAllDiv();

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressedResult, false);

	        insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
		},5000);

		window.addEventListener("keypress", checkKeyPressedResult, false);

		//check keypress
		function checkKeyPressedResult(e) {
			//check inputan user
			if (e.charCode ==  32 )  {
				// hentikan waktu
				clearTimeout(timeout_result);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressedResult, false);

				insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
			}
		}
	}
}

function insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed){
	var resultQuestion = {
		stage : "figural_classification",
		level : CURRENT_LEVEL,
		number : CURRENT_MAIN_QUESTION.sequence,
		sub_number : (CURRENT_SEQ_SUB_QUESTION+1),
		time_elapsed : time_elapsed,
		status_result : correct,
		expected : EXPECTED_ANSWER,
		answer : USER_ANSWER
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	//tambahkan counter
	CURRENT_SEQ_SUB_QUESTION++;

	// kalau counter masih dibawah jumlah total maka tampilkan sub question / pertanyaan selanjutnya
	if(CURRENT_SEQ_SUB_QUESTION < TOTAL_SUB_QUESTION){
		showQuestion();
	}else if(CURRENT_SEQ_MAIN_QUESTION < TOTAL_MAIN_QUESTION){
		//kalau counter sudah sama dengan jumlah total, maka counter main question ditambahkan
		CURRENT_SEQ_MAIN_QUESTION++;

		//tampikan main question
		GenerateAndshowMainQuestion();
	}else{
		finishTest();
	}
}

function finishTest(){
	window.localStorage.setItem('value_stage_trial_figural_classification', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan percobaan tes figural classification. Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_figural_classification'); ?>");
}