// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

var GENERATED_TEST  		= [];			//array generate number;

var ARRAY_ALL_TEST_RESULT 	= [];

var TOTAL_QUESTION			= 3;
var CURRENT_QUESTION 		= 0;
var LENGTH_DIGIT_QUESTION 	= 3;
var TIMELIMIT 				= 10000;
var COUNTER_PREPARATION		= 5

var arrayInput = [];
var stringInput = "";
var inputCounter = 0;

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

function hideAllDiv(){
	$('#div-delay').css("display","none");
	$('#div-question').css("display","none");
}

$( document ).ready(function() {
    newPage();
});

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_trial_digit_span");

	//initialize
	generateNextQuestion();

	//show question
	showQuestion();
	// ========================================================================================
}

function equalArray(a, b) {
    if (a.length === b.length) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

function generateNextQuestion(){

	//tentukan jumlah berapa digit
	if(CURRENT_QUESTION < TOTAL_QUESTION){
		
		if(ARRAY_ALL_TEST_RESULT.length < 2){
			LENGTH_DIGIT_QUESTION = 3;
		}else{
			var last_result = ARRAY_ALL_TEST_RESULT[ARRAY_ALL_TEST_RESULT.length-1];
			if(last_result.status_result == true)	{
				LENGTH_DIGIT_QUESTION++;
			}else{
				var last_2_result = ARRAY_ALL_TEST_RESULT[ARRAY_ALL_TEST_RESULT.length-2];
				if(last_2_result.status_result == false)	{
					if(LENGTH_DIGIT_QUESTION>3){
						LENGTH_DIGIT_QUESTION--;
					}
				}
			}
		}

		var question = "";
		var numbers = "";
		var last_numbers = "";
		for(var i = 0 ; i < LENGTH_DIGIT_QUESTION ; i++){
			//lakukan perulangan sampai angka berbeda dengan yang sebelumnya
			do {
			    numbers = Math.floor(Math.random() * 10);
		  	} while (numbers == last_numbers);

		  	last_numbers = numbers;

			if(i == LENGTH_DIGIT_QUESTION-1 ){
				question += numbers ;
			}else{
				question += numbers + ",";
			}
		}

		GENERATED_TEST.push(question);
	}
}

function showQuestion(){
	hideAllDiv();
	$('#div-question').css("display","block");

	//preparing
	arrayInput 		= [];
	stringInput 	= "";
	inputCounter 	= 0;
	$('#instruction-text').css("display","none");
	$('#span-input-user').text("");
	$('#span-test-question').text("");
	arrayQuestion = $.map( GENERATED_TEST[CURRENT_QUESTION].split(","), Number );
	
	//start question
	arrayQuestion.forEach((mode, index) => {
	  setTimeout(() => {
	    
	    //tampilkan soal 1 per 1
	    $('#span-test-question').text(mode);

	    if(index == arrayQuestion.length -1){

	    	//kalau udah digit terakhir, maka dalam 1 detik, ilangin isi nya dan jalankan timer
	    	setTimeout(function(){
	    		//sembunyikan semua div
				hideAllDiv();
				$('#div-delay').css("display","block");

				//setelah waktu question habis, jalankan countdown persiapan
				//munculkan waktu hitung mundur
				var counter = COUNTER_PREPARATION;
				$('#span-timer-countdown').text(counter);
				var interval = setInterval(function() {
				    counter--;
				    $('#span-timer-countdown').text(counter);
				    
				    if (counter == 0) {
				        clearInterval(interval);

				        hideAllDiv();
						$('#div-question').css("display","block");
				       
				       	$('#span-test-question').text("");

						//munculkan instruction text
			    		$('#instruction-text').css("display","block");

			    		//set timer
			    		initializeTimer();

			    		//tambahkan event listener
		    			window.addEventListener("keypress", checkKeyPressed, false);
				    }
				}, 1000);

				
			},1000);

	    	//check keypress
			function checkKeyPressed(e) {
				//jumlah input ditambah
				inputCounter++;

				//check inputan user
				if (e.charCode >=  48 && e.charCode <= 57  )  {
					switch(e.charCode){
						case 48 : arrayInput.push(0); stringInput += "0"; break;
						case 49 : arrayInput.push(1); stringInput += "1"; break;
						case 50 : arrayInput.push(2); stringInput += "2"; break;
						case 51 : arrayInput.push(3); stringInput += "3"; break;
						case 52 : arrayInput.push(4); stringInput += "4"; break;
						case 53 : arrayInput.push(5); stringInput += "5"; break;
						case 54 : arrayInput.push(6); stringInput += "6"; break;
						case 55 : arrayInput.push(7); stringInput += "7"; break;
						case 56 : arrayInput.push(8); stringInput += "8"; break;
						case 57 : arrayInput.push(9); stringInput += "9"; break;
					}

					//tampilkan hasil inputan
					$('#span-input-user').text(stringInput);

					//cek apakah sudah sejumlah panjang array
					if(inputCounter == arrayQuestion.length){
						clearTimeout(timeout);
						checkInput(arrayInput,arrayQuestion);
						window.removeEventListener("keypress", checkKeyPressed, false);
					}
				}
			}

			//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
			var timeout = setTimeout(function(){
				checkInput(arrayInput,arrayQuestion);
				window.removeEventListener("keypress", checkKeyPressed, false);
			},TIMELIMIT);

	    }
	  }, index * 1000)
	})
}

function checkInput(arrayInput,arrayQuestion){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = equalArray(arrayInput,arrayQuestion);
	if(correct){
		$('#span-input-user').text("BENAR");	
	}else{
		$('#span-input-user').text("SALAH");
	}

	var resultQuestion = {
		stage : "digit_span",
		number : (CURRENT_QUESTION+1),
		length : LENGTH_DIGIT_QUESTION,
		time_elapsed : time_elapsed,
		status_result : correct,
		question : JSON.stringify(arrayQuestion),
		answer : JSON.stringify(arrayInput)
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	$('#instruction-text').css("display","none");

	var timeout = setTimeout(function(){
		//generate next question
		CURRENT_QUESTION++;

		if(CURRENT_QUESTION < TOTAL_QUESTION){
			generateNextQuestion();
			showQuestion();
		}else{
			finishTest();
		}
	},1000);
}

function finishTest(){
	window.localStorage.setItem('value_stage_trial_digit_span', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan percobaan tes digit span. Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_digit_span'); ?>");
}