// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

// intialize data
var ARRAY_QUESTION = [
	{
	 	"level" : "trial",
	 	"sequence" : "1",
	 	"pattern" : ["9","10","12","15","19","24","30"]
	},
	{
	 	"level" : "trial",
	 	"sequence" : "2",
		"pattern" : ["13","1","3","26","-3","10","52","-7","17"]
	}
];
var angkaDeret 	= ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19","20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"]
var angkaDeret_pil = ["10", "11", "12", "13", "14", "15", "16", "17", "18", "19","20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "131", "132", "133", "134", "135", "136", "137", "148", "149", "150", "161", "162", "163", "164", "165", "166", "167", "168", "169", "170"]

var CURRENT_QUESTION 		= {};
var ARRAY_ALL_TEST_RESULT 	= [];
var TOTAL_QUESTION			= ARRAY_QUESTION.length;
var CURRENT_SEQ_QUESTION 	= 0;
var CURRENT_LEVEL 			= "";
var TIMELIMIT 				= 10000;

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

$( document ).ready(function() {
    newPage();
});

function hideAllDiv(){
	$('#div-result').css("display","none");
	$('#div-question').css("display","none");
}

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_trial_deret_angka");

	//initialize
	GenerateAndshowQuestion();
}

function random_item(items){
	return items[Math.floor(Math.random()*items.length)];
}

function GenerateAndshowQuestion(){

	//tentukan jumlah berapa digit
	if(CURRENT_SEQ_QUESTION < TOTAL_QUESTION){

		hideAllDiv();
		$('#div-question').css("display","block");
		
		//initialize
		CURRENT_QUESTION = ARRAY_QUESTION[CURRENT_SEQ_QUESTION];
		ARRAY_PATTERN_CURRENT_QUESTION = CURRENT_QUESTION.pattern;
		CURRENT_LEVEL 		 = CURRENT_QUESTION.level;
		USER_ANSWER 		 = "";
		EXPECTED_ANSWER 	 = "";
		TEMP_EXPECTED_ANSWER = "";
		ARRAY_GENERATED_FROM_PATTERN  = [];
		ARRAY_GENERATED_RANDOM_CHOICE = [];

		//ambil 1 angka random untuk inisiasi awal
		// var initial_number  = random_item(angkaDeret);
		var initial_number  = "33";

		//masukan initial number ke array generated
		ARRAY_GENERATED_FROM_PATTERN.push(initial_number);

		//tambahkan initial number ke string question
		STRING_QUESTION 	 = initial_number + "  ";

		//count & show
		for(var i = 0 ; i < ARRAY_PATTERN_CURRENT_QUESTION.length ; i++){

			//hitung digit selanjutnya
			var nextDigit = parseInt(ARRAY_GENERATED_FROM_PATTERN[i]) + parseInt(ARRAY_PATTERN_CURRENT_QUESTION[i]);

			//masukan hasil perhitungan ke array generated
			ARRAY_GENERATED_FROM_PATTERN.push(nextDigit.toString());

			//jika i bukan terakhir, maka tambahkan ke string question, kalo digit terakhir tambahkan sebagai  "..."
			if(i != ARRAY_PATTERN_CURRENT_QUESTION.length -1){
				STRING_QUESTION += nextDigit +"  ";
			}else{
				STRING_QUESTION += "... ";	

				//kalau ini digit terakhir, jadikan ini sebagai temporary expected answer
				TEMP_EXPECTED_ANSWER = nextDigit;

				//masukukan ke array generated random choice / array jawaban
				ARRAY_GENERATED_RANDOM_CHOICE.push(nextDigit);
			}
		}

		//tampilkan soal
		$("#question-text").text(STRING_QUESTION);

		//perulangan untuk mengisi array sisa jawaban 
		for(var i = 0 ; i < 4 ; i++){
			ARRAY_GENERATED_RANDOM_CHOICE.push(random_item(angkaDeret_pil));
		}

		//acak array jawaban
		ARRAY_GENERATED_RANDOM_CHOICE=_.shuffle(ARRAY_GENERATED_RANDOM_CHOICE); 

		// tampilkan array jawaban
		for(var i = 0 ; i < ARRAY_GENERATED_RANDOM_CHOICE.length ; i++){

			var temp = ARRAY_GENERATED_RANDOM_CHOICE[i];

			//tampilkan ke img
			$("#span-question-"+(i+1)).text(temp);

			//ambil nomor 1/2/3/4/5 jawaban yang benar
			if(temp == TEMP_EXPECTED_ANSWER){
				EXPECTED_ANSWER = (i+1);
			}
		}

		//set timer
		initializeTimer();

		//tambahkan event listener
		window.addEventListener("keypress", checkKeyPressed, false);

		//check keypress
		function checkKeyPressed(e) {
			//check inputan user
			if (e.charCode >=  49 && e.charCode <= 53  )  {
				switch(e.charCode){
					case 49 : USER_ANSWER = "1"; break;
					case 50 : USER_ANSWER = "2"; break;
					case 51 : USER_ANSWER = "3"; break;
					case 52 : USER_ANSWER = "4"; break;
					case 53 : USER_ANSWER = "5"; break;
				}

				// hentikan waktu
				clearTimeout(timeout);

				//cek jawaban
				checkInput(USER_ANSWER,EXPECTED_ANSWER);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressed, false);

			}
		}

		//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
		var timeout = setTimeout(function(){
			//cek jawaban
			checkInput(USER_ANSWER,EXPECTED_ANSWER);

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressed, false);
		},TIMELIMIT);
	}	
}

function checkInput(USER_ANSWER,EXPECTED_ANSWER){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = false;
	if(USER_ANSWER == EXPECTED_ANSWER){
		correct = true;

		hideAllDiv();
		$('#div-result').css("display","block");
		$('#div-result-correct').css("display","block");
		$('#div-result-incorrect').css("display","none");
		
		//ada jeda waktu 10 detik sebelum lanjut soal berikutnya
		var timeout_result = setTimeout(function(){
        	//sembunyikan semua div
			hideAllDiv();
			
			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressedResult, false);

	        insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
		},5000);

		window.addEventListener("keypress", checkKeyPressedResult, false);

		//check keypress
		function checkKeyPressedResult(e) {
			//check inputan user
			if (e.charCode ==  32 )  {
				// hentikan waktu
				clearTimeout(timeout_result);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressedResult, false);

				insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
			}
		}

	}else{
		correct = false;

		hideAllDiv();
		$('#div-result').css("display","block");
		$('#div-result-correct').css("display","none");
		$('#div-result-incorrect').css("display","block");
		

		//ada jeda waktu 10 detik sebelum lanjut soal berikutnya
		var timeout_result = setTimeout(function(){
        	//sembunyikan semua div
			hideAllDiv();

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressedResult, false);

	        insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
		},5000);

		window.addEventListener("keypress", checkKeyPressedResult, false);

		//check keypress
		function checkKeyPressedResult(e) {
			//check inputan user
			if (e.charCode ==  32 )  {
				// hentikan waktu
				clearTimeout(timeout_result);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressedResult, false);

				insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
			}
		}
	}
}

function insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed){
	var resultQuestion = {
		stage : "deret_angka",
		level : CURRENT_LEVEL,
		number : CURRENT_QUESTION.sequence,
		time_elapsed : time_elapsed,
		status_result : correct,
		expected : EXPECTED_ANSWER,
		answer : USER_ANSWER
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	CURRENT_SEQ_QUESTION++;

	if(CURRENT_SEQ_QUESTION < TOTAL_QUESTION){
		GenerateAndshowQuestion();
	}else{
		finishTest();
	}
}

function finishTest(){
	window.localStorage.setItem('value_stage_trial_deret_angka', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan percobaan tes deret angka . Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_deret_angka'); ?>");
}