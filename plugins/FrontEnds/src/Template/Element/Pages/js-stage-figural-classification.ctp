// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

var URL_IMAGE_PATH = "<?php echo $this->Url->build('/').'FrontEnds/img/figural_classification/' ?>";

// intialize data
//done
var ARRAY_QUESTION = [
	//easy
	{
	 "level" : "easy",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"easy/easy_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"easy/easy_1a.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_1b.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_1c.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1d.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1e.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_1f.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1g.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_1h.png","answer":1},
		]
	},
	{
	 "level" : "easy",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"easy/easy_2.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"easy/easy_2a.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_2b.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_2c.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_2d.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_2e.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_2f.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_2g.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_2h.png","answer":2},
		]
	},
	{
	 "level" : "easy",
	 "sequence" : "3",
	 "path" : URL_IMAGE_PATH+"easy/easy_3.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"easy/easy_3a.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_3b.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_3c.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_3d.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_3e.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_3f.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_3g.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_3h.png","answer":1},
		]
	},
	{
	 "level" : "easy",
	 "sequence" : "4",
	 "path" : URL_IMAGE_PATH+"easy/easy_4.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"easy/easy_4a.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_4b.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_4c.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_4d.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_4e.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_4f.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_4g.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_4h.png","answer":2},
		]
	},
	{
	 "level" : "easy",
	 "sequence" : "5",
	 "path" : URL_IMAGE_PATH+"easy/easy_5.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"easy/easy_5a.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_5b.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_5c.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_5d.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_5e.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_5f.png","answer":2},
			{"path":URL_IMAGE_PATH+"easy/easy_5g.png","answer":1},
			{"path":URL_IMAGE_PATH+"easy/easy_5h.png","answer":1},
		]
	},
	//medium
	{
	 "level" : "medium",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"medium/medium_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_1a.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1b.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_1c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_1d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_1f.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_1h.png","answer":2},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"medium/medium_2.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_2a.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_2b.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_2c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_2d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_2e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_2f.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_2g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_2h.png","answer":2},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "3",
	 "path" : URL_IMAGE_PATH+"medium/medium_3.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_3a.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_3b.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_3c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_3d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_3e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_3f.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_3g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_3h.png","answer":2},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "4",
	 "path" : URL_IMAGE_PATH+"medium/medium_4.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_4a.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_4b.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_4c.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_4d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_4e.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_4f.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_4g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_4h.png","answer":1},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "5",
	 "path" : URL_IMAGE_PATH+"medium/medium_5.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_5a.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_5b.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_5c.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_5d.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_5e.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_5f.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_5g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_5h.png","answer":1},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "6",
	 "path" : URL_IMAGE_PATH+"medium/medium_6.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_6a.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_6b.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_6c.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_6d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_6e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_6f.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_6g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_6h.png","answer":1},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "7",
	 "path" : URL_IMAGE_PATH+"medium/medium_7.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_7a.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_7b.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_7c.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_7d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_7e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_7f.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_7g.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_7h.png","answer":2},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "8",
	 "path" : URL_IMAGE_PATH+"medium/medium_8.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_8a.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_8b.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_8c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_8d.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_8e.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_8f.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_8g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_8h.png","answer":2},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "9",
	 "path" : URL_IMAGE_PATH+"medium/medium_9.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_9a.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_9b.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_9c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_9d.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_9e.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_9f.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_9g.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_9h.png","answer":1},
		]
	},
	{
	 "level" : "medium",
	 "sequence" : "10",
	 "path" : URL_IMAGE_PATH+"medium/medium_10.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"medium/medium_10a.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_10b.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_10c.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_10d.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_10e.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_10f.png","answer":1},
			{"path":URL_IMAGE_PATH+"medium/medium_10g.png","answer":2},
			{"path":URL_IMAGE_PATH+"medium/medium_10h.png","answer":1},
		]
	},
	//hard
	{
	 "level" : "hard",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"hard/hard_1.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"hard/hard_1a.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_1b.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_1c.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_1d.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_1e.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_1f.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_1g.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_1h.png","answer":3},
		]
	},
	{
	 "level" : "hard",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"hard/hard_2.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"hard/hard_2a.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_2b.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_2c.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_2d.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_2e.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_2f.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_2g.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_2h.png","answer":2},
		]
	},
	{
	 "level" : "hard",
	 "sequence" : "3",
	 "path" : URL_IMAGE_PATH+"hard/hard_3.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"hard/hard_3a.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_3b.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_3c.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_3d.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_3e.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_3f.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_3g.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_3h.png","answer":1},
		]
	},
	{
	 "level" : "hard",
	 "sequence" : "4",
	 "path" : URL_IMAGE_PATH+"hard/hard_4.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"hard/hard_4a.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_4b.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_4c.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_4d.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_4e.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_4f.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_4g.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_4h.png","answer":1},
		]
	},
	{
	 "level" : "hard",
	 "sequence" : "5",
	 "path" : URL_IMAGE_PATH+"hard/hard_5.png",
	 "question" : 
		[
			{"path":URL_IMAGE_PATH+"hard/hard_5a.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_5b.png","answer":1},
			{"path":URL_IMAGE_PATH+"hard/hard_5c.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_5d.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_5e.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_5f.png","answer":3},
			{"path":URL_IMAGE_PATH+"hard/hard_5g.png","answer":2},
			{"path":URL_IMAGE_PATH+"hard/hard_5h.png","answer":2},
		]
	}
];

var ARRAY_ALL_TEST_RESULT 	= [];

var TOTAL_MAIN_QUESTION		= ARRAY_QUESTION.length;
var CURRENT_SEQ_MAIN_QUESTION 	= 0;
var CURRENT_MAIN_QUESTION 	= {};

var ARRAY_CURRENT_SUB_QUESTION 	= [];
var TOTAL_SUB_QUESTION			= 0;
var CURRENT_SEQ_SUB_QUESTION 	= 0;
var CURRENT_SUB_QUESTION 		= {};

var CURRENT_LEVEL 			= "";
var TIMELIMIT_ANSWER 		= 10000;
var TIMELIMIT_MEMORIZATION 	= 10000;
var COUNTER_PREPARATION		= 5

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

function hideAllDiv(){
	$('#div-question').css("display","none");
	$('#div-result').css("display","none");
}

$( document ).ready(function() {
    newPage();
});

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_figural_classification");

	//initialize
	GenerateAndshowMainQuestion();
}

function GenerateAndshowMainQuestion(){
	//tentukan jumlah berapa digit
	if(CURRENT_SEQ_MAIN_QUESTION < TOTAL_MAIN_QUESTION){
		
		//set main
		CURRENT_MAIN_QUESTION 		= ARRAY_QUESTION[CURRENT_SEQ_MAIN_QUESTION];
		CURRENT_LEVEL 		 		= CURRENT_MAIN_QUESTION.level;

		//set sub
		ARRAY_CURRENT_SUB_QUESTION 	= CURRENT_MAIN_QUESTION.question;
		TOTAL_SUB_QUESTION 			= ARRAY_CURRENT_SUB_QUESTION.length;
		CURRENT_SEQ_SUB_QUESTION 	= 0;
		
		//sembunyikan semua div
		hideAllDiv();

		$('#div-question').css("display","block");
		$("#img-question").attr("src",CURRENT_MAIN_QUESTION.path);

		if(CURRENT_MAIN_QUESTION.level == "hard"){
			$("#instruction-text-1").text("Kelompokkan gambar di bawah ke dalam grup 1 / grup 2 / grup 3");
			$("#instruction-text-2").text("TEKAN ANGKA 1 / 2 / 3 UNTUK MEMILIH JAWABAN");
		}else{
			$("#instruction-text-1").text("Kelompokkan gambar di bawah ke dalam grup 1 / grup 2");
			$("#instruction-text-2").text("TEKAN ANGKA 1 / 2 UNTUK MEMILIH JAWABAN");
		}

        showQuestion();

	}else{
		finishTest();
	}
}

function showQuestion(){
	if(CURRENT_SEQ_SUB_QUESTION < TOTAL_SUB_QUESTION){

		//sembunyikan semua div
		hideAllDiv();

		//tampilkan div question
        $('#div-question').css("display","block");

		//initialize
		var USER_ANSWER 		 = "";
		var EXPECTED_ANSWER 	 = "";

		//ambil soal berikutnya
		CURRENT_SUB_QUESTION = ARRAY_CURRENT_SUB_QUESTION[CURRENT_SEQ_SUB_QUESTION];

		$("#img-question-1").attr("src",CURRENT_SUB_QUESTION.path);
		EXPECTED_ANSWER = CURRENT_SUB_QUESTION.answer;

		//jalankan waktu stopwatch
		initializeTimer();

		//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
		var timeout = setTimeout(function(){
			// hentikan waktu
			clearTimeout(timeout);

			//cek jawaban
			checkInput(USER_ANSWER,EXPECTED_ANSWER);

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressed, false);
		},TIMELIMIT_ANSWER);

		//tambahkan event listener
		window.addEventListener("keypress", checkKeyPressed, false);

		//check keypress
		function checkKeyPressed(e) {

			//check inputan user, sesuai range ngga
			var USER_INPUT_VALID = false;

			if(CURRENT_LEVEL == "hard"){
				if (e.charCode >=  49 && e.charCode <= 51  )  {
					switch(e.charCode){
						case 49 : USER_ANSWER = "1"; break;
						case 50 : USER_ANSWER = "2"; break;
						case 51 : USER_ANSWER = "3"; break;
					}
					USER_INPUT_VALID = true;
				}
			}else if (CURRENT_LEVEL == "easy" || CURRENT_LEVEL == "medium"){
				if (e.charCode >=  49 && e.charCode <= 50  )  {
					switch(e.charCode){
						case 49 : USER_ANSWER = "1"; break;
						case 50 : USER_ANSWER = "2"; break;
					}
					USER_INPUT_VALID = true;
				}
			}

			if(USER_INPUT_VALID == true){
				// hentikan waktu
				clearTimeout(timeout);

				//cek jawaban
				checkInput(USER_ANSWER,EXPECTED_ANSWER);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressed, false);
			}
		}
	}else{
		CURRENT_SEQ_MAIN_QUESTION++;
		GenerateAndshowMainQuestion();
	}
}

function checkInput(USER_ANSWER,EXPECTED_ANSWER){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = false;
	if(USER_ANSWER == EXPECTED_ANSWER){
		correct = true;
	}else{
		correct = false;
	}

	hideAllDiv();
    insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
}

function insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed){
	var resultQuestion = {
		stage : "figural_classification",
		level : CURRENT_LEVEL,
		number : CURRENT_MAIN_QUESTION.sequence,
		sub_number : (CURRENT_SEQ_SUB_QUESTION+1),
		time_elapsed : time_elapsed,
		status_result : correct,
		expected : EXPECTED_ANSWER,
		answer : USER_ANSWER
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	//tambahkan counter
	CURRENT_SEQ_SUB_QUESTION++;

	// kalau counter masih dibawah jumlah total maka tampilkan sub question / pertanyaan selanjutnya
	if(CURRENT_SEQ_SUB_QUESTION < TOTAL_SUB_QUESTION){
		showQuestion();
	}else if(CURRENT_SEQ_MAIN_QUESTION < TOTAL_MAIN_QUESTION){
		//kalau counter sudah sama dengan jumlah total, maka counter main question ditambahkan
		CURRENT_SEQ_MAIN_QUESTION++;

		//tampikan main question
		GenerateAndshowMainQuestion();
	}else{
		finishTest();
	}
}

function finishTest(){
	window.localStorage.setItem('value_stage_figural_classification', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan tes figural classification. Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_trial_deret_angka'); ?>");
}