// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

var URL_IMAGE_PATH = "<?php echo $this->Url->build('/').'FrontEnds/img/building_memory/' ?>";

// intialize data
var ARRAY_QUESTION = [
	{
		"level" : "easy",
		"sequence" : "1",
		"path" : URL_IMAGE_PATH+"easy/easy_1.png",
		"frame" : URL_IMAGE_PATH+"easy/easy_1_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"easy/easy_1_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":true}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"easy/easy_1_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":true},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"easy/easy_1_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false}
					]
			},
		]
	},
	{
		"level" : "easy",
		"sequence" : "2",
		"path" : URL_IMAGE_PATH+"easy/easy_2.png",
		"frame" : URL_IMAGE_PATH+"easy/easy_2_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"easy/easy_2_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":true},
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"easy/easy_2_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":true},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"easy/easy_2_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":true}
					]
			},
		]
	},

	//medium
	{
		"level" : "medium",
		"sequence" : "1",
		"path" : URL_IMAGE_PATH+"medium/medium_1.png",
		"frame" : URL_IMAGE_PATH+"medium/medium_1_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"medium/medium_1_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":true},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"medium/medium_1_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"medium/medium_1_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":true},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
		]
	},
	{
		"level" : "medium",
		"sequence" : "2",
		"path" : URL_IMAGE_PATH+"medium/medium_2.png",
		"frame" : URL_IMAGE_PATH+"medium/medium_2_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"medium/medium_2_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"medium/medium_2_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":true},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"medium/medium_2_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":true},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":false}
					]
			},
		]
	},

	//hard
	{
		"level" : "hard",
		"sequence" : "1",
		"path" : URL_IMAGE_PATH+"hard/hard_1.png",
		"frame" : URL_IMAGE_PATH+"hard/hard_1_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"j.png","answer":false},
						{"path":URL_IMAGE_PATH+"l.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":true},
						{"path":URL_IMAGE_PATH+"g.png","answer":false},
						{"path":URL_IMAGE_PATH+"l.png","answer":false}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":true},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"i.png","answer":false},
						{"path":URL_IMAGE_PATH+"k.png","answer":false}
					]
			},
			{
				"sub_sequence" : "4",
				"path" : URL_IMAGE_PATH+"hard/hard_1_sub_4.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"g.png","answer":true},
						{"path":URL_IMAGE_PATH+"h.png","answer":false},
						{"path":URL_IMAGE_PATH+"k.png","answer":false}
					]
			},
		]
	},
	{
		"level" : "hard",
		"sequence" : "2",
		"path" : URL_IMAGE_PATH+"hard/hard_2.png",
		"frame" : URL_IMAGE_PATH+"hard/hard_2_frame.png",
		"question" : 
		[
			{
				"sub_sequence" : "1",
				"path" : URL_IMAGE_PATH+"hard/hard_2_sub_1.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":true},
						{"path":URL_IMAGE_PATH+"f.png","answer":false},
						{"path":URL_IMAGE_PATH+"h.png","answer":false},
						{"path":URL_IMAGE_PATH+"i.png","answer":false},
						{"path":URL_IMAGE_PATH+"j.png","answer":false}
					]
			},
			{
				"sub_sequence" : "2",
				"path" : URL_IMAGE_PATH+"hard/hard_2_sub_2.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":false},
						{"path":URL_IMAGE_PATH+"j.png","answer":false},
						{"path":URL_IMAGE_PATH+"k.png","answer":false},
						{"path":URL_IMAGE_PATH+"l.png","answer":true}
					]
			},
			{
				"sub_sequence" : "3",
				"path" : URL_IMAGE_PATH+"hard/hard_2_sub_3.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"b.png","answer":false},
						{"path":URL_IMAGE_PATH+"d.png","answer":false},
						{"path":URL_IMAGE_PATH+"e.png","answer":true},
						{"path":URL_IMAGE_PATH+"g.png","answer":false},
						{"path":URL_IMAGE_PATH+"k.png","answer":false}
					]
			},
			{
				"sub_sequence" : "4",
				"path" : URL_IMAGE_PATH+"hard/hard_2_sub_4.png",
				"choice" : 
					[
						{"path":URL_IMAGE_PATH+"a.png","answer":false},
						{"path":URL_IMAGE_PATH+"c.png","answer":false},
						{"path":URL_IMAGE_PATH+"j.png","answer":true},
						{"path":URL_IMAGE_PATH+"l.png","answer":false},
						{"path":URL_IMAGE_PATH+"m.png","answer":false}
					]
			},
		]
	},
];

var ARRAY_ALL_TEST_RESULT 	= [];

var TOTAL_MAIN_QUESTION		= ARRAY_QUESTION.length;
var CURRENT_SEQ_MAIN_QUESTION 	= 0;
var CURRENT_MAIN_QUESTION 	= {};

var ARRAY_CURRENT_SUB_QUESTION 	= [];
var TOTAL_SUB_QUESTION			= 0;
var CURRENT_SEQ_SUB_QUESTION 	= 0;
var CURRENT_SUB_QUESTION 		= {};

var CURRENT_LEVEL 			= "";
var TIMELIMIT_ANSWER 		= 10000;
var TIMELIMIT_MEMORIZATION 	= 10000;
var COUNTER_PREPARATION		= 5

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

function hideAllDiv(){
	$('#div-memorization').css("display","none");
	$('#div-delay').css("display","none");
	$('#div-question').css("display","none");
	$('#div-correct').css("display","none");
}

$( document ).ready(function() {
    newPage();
});

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_building_memory");

	//initialize
	GenerateAndshowMainQuestion();
}

function GenerateAndshowMainQuestion(){
	//tentukan jumlah berapa digit
	if(CURRENT_SEQ_MAIN_QUESTION < TOTAL_MAIN_QUESTION){
		
		//set main
		CURRENT_MAIN_QUESTION 		= ARRAY_QUESTION[CURRENT_SEQ_MAIN_QUESTION];
		CURRENT_LEVEL 		 		= CURRENT_MAIN_QUESTION.level;

		//set sub
		ARRAY_CURRENT_SUB_QUESTION 	= CURRENT_MAIN_QUESTION.question;
		TOTAL_SUB_QUESTION 			= ARRAY_CURRENT_SUB_QUESTION.length;
		CURRENT_SEQ_SUB_QUESTION 	= 0;
		
		//sembunyikan semua div
		hideAllDiv();

		//set img src memorization
		$('#div-memorization').css("display","block");
		$("#img-memorization").attr("src",CURRENT_MAIN_QUESTION.path);

		//set timer waktu memorization
		setTimeout(function(){
			//sembunyikan semua div
			hideAllDiv();

			//setelah waktu memorization habis, jalankan countdown persiapan
			$('#div-delay').css("display","block");
			
			//munculkan waktu hitung mundur
			var counter = COUNTER_PREPARATION;
			$('#span-timer-countdown').text(counter);
			var interval = setInterval(function() {
			    counter--;
			    $('#span-timer-countdown').text(counter);
			    
			    if (counter == 0) {
			        clearInterval(interval);
			        showQuestion();
			    }
			}, 1000);

		},TIMELIMIT_MEMORIZATION);

	}else{
		finishTest();
	}
}

function showQuestion(){
	if(CURRENT_SEQ_SUB_QUESTION < TOTAL_SUB_QUESTION){

		//sembunyikan semua div
		hideAllDiv();

		//tampilkan div question
        $('#div-question').css("display","block");

		//initialize
		var USER_ANSWER 		 = "";
		var EXPECTED_ANSWER 	 = "";

		//set user input empty	
		$('#span-input-user').text("");

		//ambil soal berikutnya
		CURRENT_SUB_QUESTION = ARRAY_CURRENT_SUB_QUESTION[CURRENT_SEQ_SUB_QUESTION];

		//ambil pilihan abcde
		var ARRAY_CHOICE_CURRENT_QUESTION = CURRENT_SUB_QUESTION.choice;

		//random pilihan abcde
		ARRAY_CHOICE_CURRENT_QUESTION=_.shuffle(ARRAY_CHOICE_CURRENT_QUESTION); 

		//tampilkan ke img
		$("#img-question").attr("src",CURRENT_SUB_QUESTION.path);
		$("#img-question-frame").attr("src",CURRENT_MAIN_QUESTION.frame);

		//show image abcde
		for(var i = 0 ; i < ARRAY_CHOICE_CURRENT_QUESTION.length ; i++){

			var temp = ARRAY_CHOICE_CURRENT_QUESTION[i];

			//tampilkan ke img
			$("#img-question-"+(i+1)).attr("src",temp.path);

			//ambil jawaban yang benar
			if(temp.answer == true){
				EXPECTED_ANSWER = (i+1);
			}
		}

		//jalankan waktu stopwatch
		initializeTimer();

		//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
		var timeout = setTimeout(function(){
			// hentikan waktu
			clearTimeout(timeout);

			//cek jawaban
			checkInput(USER_ANSWER,EXPECTED_ANSWER);

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressed, false);
		},TIMELIMIT_ANSWER);

		//tambahkan event listener
		window.addEventListener("keypress", checkKeyPressed, false);

		//check keypress
		function checkKeyPressed(e) {
			//check inputan user
			if (e.charCode >=  49 && e.charCode <= 53  )  {
				switch(e.charCode){
					case 49 : USER_ANSWER = "1"; break;
					case 50 : USER_ANSWER = "2"; break;
					case 51 : USER_ANSWER = "3"; break;
					case 52 : USER_ANSWER = "4"; break;
					case 53 : USER_ANSWER = "5"; break;
				}

				//tampilkan hasil inputan
				$('#span-input-user').text(USER_ANSWER);

				// hentikan waktu
				clearTimeout(timeout);

				//cek jawaban
				checkInput(USER_ANSWER,EXPECTED_ANSWER);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressed, false);
			}
		}
	}else{
		CURRENT_SEQ_MAIN_QUESTION++;
		GenerateAndshowMainQuestion();
	}
}

function checkInput(USER_ANSWER,EXPECTED_ANSWER){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = false;
	if(USER_ANSWER == EXPECTED_ANSWER){
		correct = true;
	}else{
		correct = false;
	}

	insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
}

function insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed){
	var resultQuestion = {
		stage : "building_memory",
		level : CURRENT_LEVEL,
		number : CURRENT_MAIN_QUESTION.sequence,
		sub_number : (CURRENT_SEQ_SUB_QUESTION+1),
		time_elapsed : time_elapsed,
		status_result : correct,
		expected : EXPECTED_ANSWER,
		answer : USER_ANSWER
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	//tambahkan counter
	CURRENT_SEQ_SUB_QUESTION++;

	// kalau counter masih dibawah jumlah total maka tampilkan sub question / pertanyaan selanjutnya
	if(CURRENT_SEQ_SUB_QUESTION < TOTAL_SUB_QUESTION){
		showQuestion();
	}else if(CURRENT_SEQ_MAIN_QUESTION < TOTAL_MAIN_QUESTION){
		//kalau counter sudah sama dengan jumlah total, maka counter main question ditambahkan
		CURRENT_SEQ_MAIN_QUESTION++;

		//tampikan main question
		GenerateAndshowMainQuestion();
	}else{
		finishTest();
	}
}

function finishTest(){
	window.localStorage.setItem('value_stage_building_memory', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan tes building memory. Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_trial_shape_classification'); ?>");
}