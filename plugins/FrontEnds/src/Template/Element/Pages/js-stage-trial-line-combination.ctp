// var VALUE_REGISTRATION = localStorage.getItem("value_registration");
// var VALUE_TRIAL_1 = localStorage.getItem("value_trial_1");

// if(VALUE_REGISTRATION == null){
// 	alert("Anda harus mengisi form identitas terlebih dahulu");
//     window.location.replace("<?php echo $this->Url->build('/register'); ?>");
// }else if(VALUE_TRIAL_1 == null){
// 	alert("Anda harus menyelesaikan percobaan terlebih dahulu");
// 	window.location.replace("<?php echo $this->Url->build('/instruction_trial_one'); ?>");
// }else{
// 	VALUE_REGISTRATION = JSON.parse(localStorage.getItem("value_registration"));
// 	VALUE_TRIAL_1 = JSON.parse(localStorage.getItem("value_trial_1"));
// }

var URL_IMAGE_PATH = "<?php echo $this->Url->build('/').'FrontEnds/img/line_combination/' ?>";

// intialize data
var ARRAY_QUESTION = [
	{
	 "level" : "trial",
	 "sequence" : "1",
	 "path" : URL_IMAGE_PATH+"trial/trial_1.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_1_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_1a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_1d.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_1e.png","answer":false}]
	},
	{
	 "level" : "trial",
	 "sequence" : "2",
	 "path" : URL_IMAGE_PATH+"trial/trial_2.png",
	 "expected" : URL_IMAGE_PATH+"trial/trial_2_expected.png",
	 "choice" : 
	[{"path":URL_IMAGE_PATH+"trial/trial_2a.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2b.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2c.png","answer":false},{"path":URL_IMAGE_PATH+"trial/trial_2d.png","answer":true},{"path":URL_IMAGE_PATH+"trial/trial_2e.png","answer":false}]
	}
];

var ARRAY_ALL_TEST_RESULT 	= [];
var TOTAL_QUESTION			= ARRAY_QUESTION.length;
var CURRENT_SEQ_QUESTION 	= 0;
var CURRENT_LEVEL 			= "";
var TIMELIMIT 				= 10000;
var CURRENT_QUESTION 		= {};

// ============================== STOPWATCH START =====================================

var additionalTime = 0;
var currentTime = 0;
var startTime = '';
var timer = 0;

function addZeros(number, length) {
	var string = '' + number; // Int to string.
	while (string.length < length) { string = '0' + string; }
	return string;
}

function now() {
	return (new Date().getTime());
}

function transformMillisecondsToFormattedTimeAndPrint(time) { // Time in milliseconds.
	var hours = parseInt(time / 3600000);
	var minutes = parseInt(time / 60000) - (hours * 60);
	var seconds = parseInt(time / 1000) - (minutes * 60) - (hours * 3600);
	var milliseconds = parseInt(time % 1000);
	$('#timer').text(addZeros(hours, 2) + ':' + addZeros(minutes, 2) + ':' + addZeros(seconds, 2) + '.' + addZeros(milliseconds, 3));	
}

function resetTimer(){
	//pause 
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	//clear
	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');

	//resume
	startTimer();
}

function pauseTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);
}

function stopTimer(){
	clearInterval(timer);
	$('#additionalTime').text(currentTime);

	$('#additionalTime').text('0'); // Delete any additional time.
	$('#timer').text('00:00:00.000');
}

function startTimer() {
	additionalTime = 0;
	currentTime = 0;
	startTime = now();

	timer = setInterval(function() {
		var additionalTime = parseInt($('#additionalTime').text());
		currentTime = (now() - startTime) + additionalTime;
		transformMillisecondsToFormattedTimeAndPrint(currentTime);
	}, 55); // in millisecond.
}

function initializeTimer(){
	var additionalTime = $('#additionalTime').text();
	transformMillisecondsToFormattedTimeAndPrint(additionalTime);

	startTimer();
}

// ============================== STOPWATCH END =====================================

$( document ).ready(function() {
    newPage();
});

function newPage(){
	//bersihkan data local storage
	window.localStorage.removeItem("value_stage_trial_line_combination");

	//initialize
	GenerateAndshowQuestion();
}

function hideAllDiv(){
	$('#div-result').css("display","none");
	$('#div-question').css("display","none");
}

function GenerateAndshowQuestion(){

	//tentukan jumlah berapa digit
	if(CURRENT_SEQ_QUESTION < TOTAL_QUESTION){

		hideAllDiv();
		$('#div-question').css("display","block");
		
		//initialize
		CURRENT_QUESTION 	 = ARRAY_QUESTION[CURRENT_SEQ_QUESTION];
		CURRENT_LEVEL 		 = CURRENT_QUESTION.level;
		USER_ANSWER 		 = "";
		EXPECTED_ANSWER 	 = "";

		//set user input empty
		$('#span-input-user').text("");

		//set img src soal
		$("#img-question").attr("src",CURRENT_QUESTION.path);

		//get array question
		ARRAY_CHOICE_CURRENT_QUESTION = CURRENT_QUESTION.choice;
		ARRAY_CHOICE_CURRENT_QUESTION=_.shuffle(ARRAY_CHOICE_CURRENT_QUESTION); 

		//show image
		for(var i = 0 ; i < ARRAY_CHOICE_CURRENT_QUESTION.length ; i++){

			var temp = ARRAY_CHOICE_CURRENT_QUESTION[i];

			//tampilkan ke img
			$("#img-question-"+(i+1)).attr("src",temp.path);

			//ambil jawaban yang benar
			if(temp.answer == true){
				EXPECTED_ANSWER = (i+1);
			}
		}

		//set timer
		initializeTimer();

		//tambahkan event listener
		window.addEventListener("keypress", checkKeyPressed, false);

		//check keypress
		function checkKeyPressed(e) {
			//check inputan user
			if (e.charCode >=  49 && e.charCode <= 53  )  {
				switch(e.charCode){
					case 49 : USER_ANSWER = "1"; break;
					case 50 : USER_ANSWER = "2"; break;
					case 51 : USER_ANSWER = "3"; break;
					case 52 : USER_ANSWER = "4"; break;
					case 53 : USER_ANSWER = "5"; break;
				}

				//tampilkan hasil inputan
				$('#span-input-user').text(USER_ANSWER);

				// hentikan waktu
				clearTimeout(timeout);

				//cek jawaban
				checkInput(USER_ANSWER,EXPECTED_ANSWER);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressed, false);
			}
		}

		//waktu pengerjaan , setelah itu matikan input keyboard dan cek result
		var timeout = setTimeout(function(){
			//cek jawaban
			checkInput(USER_ANSWER,EXPECTED_ANSWER);

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressed, false);
		},TIMELIMIT);
	}	
}

function checkInput(USER_ANSWER,EXPECTED_ANSWER){
	//stop stopwatch
	var time_html = $('#timer').html();
	var time_html = time_html.split(".");
	var time = time_html[0].split(':');
	var seconds = (+time[0]) * 60 * 60 + (+time[1]) * 60 + (+time[2]); 
	var time_elapsed = parseFloat(seconds + "." +time_html[1]);
	stopTimer();

	var correct = false;
	if(USER_ANSWER == EXPECTED_ANSWER){
		correct = true;

		hideAllDiv();
		$('#div-result').css("display","block");
		$('#div-result-correct').css("display","block");
		$('#div-result-incorrect').css("display","none");
		
		//ada jeda waktu 10 detik sebelum lanjut soal berikutnya
		var timeout_result = setTimeout(function(){
        	//sembunyikan semua div
			hideAllDiv();
			
			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressedResult, false);

	        insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
		},50000);

		window.addEventListener("keypress", checkKeyPressedResult, false);

		//check keypress
		function checkKeyPressedResult(e) {
			//check inputan user
			if (e.charCode ==  32 )  {
				// hentikan waktu
				clearTimeout(timeout_result);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressedResult, false);

				insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
			}
		}

	}else{
		correct = false;

		hideAllDiv();
		$('#div-result').css("display","block");
		$('#div-result-correct').css("display","none");
		$('#div-result-incorrect').css("display","block");
		
		$("#img-expected").attr("src",CURRENT_QUESTION.expected);

		//ada jeda waktu 10 detik sebelum lanjut soal berikutnya
		var timeout_result = setTimeout(function(){
        	//sembunyikan semua div
			hideAllDiv();

			//hilangkan event listener
			window.removeEventListener("keypress", checkKeyPressedResult, false);

	        insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
		},50000);

		window.addEventListener("keypress", checkKeyPressedResult, false);

		//check keypress
		function checkKeyPressedResult(e) {
			//check inputan user
			if (e.charCode ==  32 )  {
				// hentikan waktu
				clearTimeout(timeout_result);

				//hilangkan event listener
				window.removeEventListener("keypress", checkKeyPressedResult, false);

				insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed);
			}
		}
	}
}

function insertDataInputToArray(USER_ANSWER,EXPECTED_ANSWER,correct,time_elapsed){
	var resultQuestion = {
		stage : "line_combination",
		level : CURRENT_LEVEL,
		number : CURRENT_QUESTION.sequence,
		time_elapsed : time_elapsed,
		status_result : correct,
		expected : EXPECTED_ANSWER,
		answer : USER_ANSWER
	}

	ARRAY_ALL_TEST_RESULT.push(resultQuestion);

	//generate next question
	CURRENT_SEQ_QUESTION++;

	if(CURRENT_SEQ_QUESTION < TOTAL_QUESTION){
		GenerateAndshowQuestion();
	}else{
		finishTest();
	}
}

function finishTest(){
	window.localStorage.setItem('value_stage_trial_line_combination', JSON.stringify(ARRAY_ALL_TEST_RESULT));

	console.log(JSON.stringify(ARRAY_ALL_TEST_RESULT));

	alert("Terima kasih sudah menyelesaikan percobaan tes line combination. Klik OK atau tekan SPACE untuk melanjutkan.");

	window.location.replace("<?php echo $this->Url->build('/instruction_line_combination'); ?>");
}