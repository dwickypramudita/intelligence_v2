<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Tes Kemampuan Kognitif">
    <title>Tes Kemampuan Kognitif</title>
    <?php
        echo $this->fetch('meta');
        echo $this->Html->css([
            '/FrontEnds/css/bootstrap.css',
            '/FrontEnds/css/cover.css',
        ]);
    ?>
    <?php
        $controller = strtolower($this->request->params['controller']);
        $action = strtolower($this->request->params['action']);

        if ($controller == 'pages') {
            if ($action == 'instructiontrialdigitspan' || $action == 'instructiontrial3drotation' || $action == 'instructiontriallinecombination' || $action == 'instructiontrialpaperfolding' || $action == 'instructiontrialbuildingmemory' || $action == 'instructiontrialshapeclassification' || $action == 'instructiontrialfiguralclassification' || $action == 'instructiontrialderetangka' ) {
                echo $this->Html->css([
                    '/FrontEnds/css/instruction-right.css',
                ]);
            }else if ($action == 'instructiondigitspan' || $action == 'instruction3drotation' || $action == 'instructionlinecombination' || $action == 'instructionpaperfolding' || $action == 'instructionbuildingmemory' || $action == 'instructionshapeclassification' || $action == 'instructionfiguralclassification' || $action == 'instructionderetangka') {
                echo $this->Html->css([
                    '/FrontEnds/css/instruction-center.css',
                ]);
            }
        } 
    ?>
</head>
<body>
    <?php echo $this->fetch('content'); ?>
    <?php echo $this->Html->script([
        '/FrontEnds/js/jquery-3.5.1.min.js',
    ]); ?>
    <script type="text/javascript">
        <?php
            $controller = strtolower($this->request->params['controller']);
            $action = strtolower($this->request->params['action']);

            if ($controller == 'pages') {
                if ($action == 'instructiontrialdigitspan') {
                    echo $this->element('Pages/js-instruction-trial-digit-span');
                }else if ($action == 'instructiondigitspan') {
                    echo $this->element('Pages/js-instruction-digit-span');
                }

                else if ($action == 'instructiontrial3drotation') {
                    echo $this->element('Pages/js-instruction-trial-3d-rotation');
                }else if ($action == 'instruction3drotation') {
                    echo $this->element('Pages/js-instruction-3d-rotation');
                }

                else if ($action == 'instructiontriallinecombination') {
                    echo $this->element('Pages/js-instruction-trial-line-combination');
                }else if ($action == 'instructionlinecombination') {
                    echo $this->element('Pages/js-instruction-line-combination');
                }

                else if ($action == 'instructiontrialpaperfolding') {
                    echo $this->element('Pages/js-instruction-trial-paper-folding');
                }else if ($action == 'instructionpaperfolding') {
                    echo $this->element('Pages/js-instruction-paper-folding');
                }

                else if ($action == 'instructiontrialbuildingmemory') {
                    echo $this->element('Pages/js-instruction-trial-building-memory');
                }else if ($action == 'instructionbuildingmemory') {
                    echo $this->element('Pages/js-instruction-building-memory');
                }

                else if ($action == 'instructiontrialshapeclassification') {
                    echo $this->element('Pages/js-instruction-trial-shape-classification');
                }else if ($action == 'instructionshapeclassification') {
                    echo $this->element('Pages/js-instruction-shape-classification');
                }

                else if ($action == 'instructiontrialfiguralclassification') {
                    echo $this->element('Pages/js-instruction-trial-figural-classification');
                }else if ($action == 'instructionfiguralclassification') {
                    echo $this->element('Pages/js-instruction-figural-classification');
                }

                else if ($action == 'instructiontrialderetangka') {
                    echo $this->element('Pages/js-instruction-trial-deret-angka');
                }else if ($action == 'instructionderetangka') {
                    echo $this->element('Pages/js-instruction-deret-angka');
                }
            } 
        ?>
    </script>
    </body>
</html>
