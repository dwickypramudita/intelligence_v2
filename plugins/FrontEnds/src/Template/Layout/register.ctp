<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Tes Kemampuan Kognitif">
    <title>Tes Kemampuan Kognitif</title>
    <?php
        echo $this->fetch('meta');
        echo $this->Html->css([
            '/FrontEnds/css/bootstrap.css',
            '/FrontEnds/css/cover.css',
            '/FrontEnds/css/register.css',
        ]);
    ?>
    <style type="text/css">
        .error{
            color:red;
        }
    </style>
</head>
<body>
    <?php echo $this->fetch('content'); ?>

    <?php echo $this->Html->script([
        '/FrontEnds/js/jquery-3.5.1.min.js',
    ]); ?>
    <script type="text/javascript">
        <?php
            $controller = strtolower($this->request->params['controller']);
            $action = strtolower($this->request->params['action']);

            if ($controller == 'pages') {
                if ($action == 'register') {
                    echo $this->element('Pages/js-register');
                }
            } 
        ?>
    </script>
    </body>
</html>
