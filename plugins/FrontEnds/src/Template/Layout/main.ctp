<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Tes Kemampuan Kognitif">
    <title>Tes Kemampuan Kognitif</title>
    <?php
        echo $this->fetch('meta');
        echo $this->Html->css([
            '/FrontEnds/css/bootstrap.css',
            '/FrontEnds/css/cover.css',
            '/FrontEnds/css/main.css',
            
            '/FrontEnds/css/flipclock.css',
            '/FrontEnds/css/common.css',
        ]);
    ?>
</head>
<body class="text-center">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <?php echo $this->element('header_main'); ?>
        <main role="main" class="inner cover" style="height: 100%;">
            <?php echo $this->fetch('content'); ?>
        </main>
        <?php echo $this->element('footer'); ?>
    </div>

    <?php echo $this->Html->script([
        '/FrontEnds/js/jquery-3.5.1.min.js',
        '/FrontEnds/js/jintervals.js',
        '/FrontEnds/js/underscore-min.js',
    ]); ?>
    <script type="text/javascript">
        <?php
            $controller = strtolower($this->request->params['controller']);
            $action = strtolower($this->request->params['action']);

            if ($controller == 'pages') {
                if ($action == 'index') {
                    echo $this->element('Pages/js-index');
                } else  if ($action == 'stagetrialdigitspan') {
                    echo $this->element('Pages/js-stage-trial-digit-span');
                } else  if ($action == 'stagetrial3drotation') {
                    echo $this->element('Pages/js-stage-trial-3d-rotation');
                } else  if ($action == 'stagetriallinecombination') {
                    echo $this->element('Pages/js-stage-trial-line-combination');
                } else  if ($action == 'stagetrialpaperfolding') {
                    echo $this->element('Pages/js-stage-trial-paper-folding');
                } else  if ($action == 'stagetrialbuildingmemory') {
                    echo $this->element('Pages/js-stage-trial-building-memory');
                } else  if ($action == 'stagetrialshapeclassification') {
                    echo $this->element('Pages/js-stage-trial-shape-classification');
                } else  if ($action == 'stagetrialfiguralclassification') {
                    echo $this->element('Pages/js-stage-trial-figural-classification');
                } else  if ($action == 'stagetrialderetangka') {
                    echo $this->element('Pages/js-stage-trial-deret-angka');

                }else  if ($action == 'stagedigitspan') {
                    echo $this->element('Pages/js-stage-digit-span');
                } else  if ($action == 'stage3drotation') {
                    echo $this->element('Pages/js-stage-3d-rotation');
                } else  if ($action == 'stagelinecombination') {
                    echo $this->element('Pages/js-stage-line-combination');
                } else  if ($action == 'stagepaperfolding') {
                    echo $this->element('Pages/js-stage-paper-folding');
                } else  if ($action == 'stagebuildingmemory') {
                    echo $this->element('Pages/js-stage-building-memory');
                } else  if ($action == 'stageshapeclassification') {
                    echo $this->element('Pages/js-stage-shape-classification');
                } else  if ($action == 'stagefiguralclassification') {
                    echo $this->element('Pages/js-stage-figural-classification');
                } else  if ($action == 'stagederetangka') {
                    echo $this->element('Pages/js-stage-deret-angka');
                } 
            } 
        ?>
    </script>
    </body>
</html>
