<?php
namespace FrontEnds\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Settings Model
 *
 * @method \FrontEnds\Model\Entity\Setting get($primaryKey, $options = [])
 * @method \FrontEnds\Model\Entity\Setting newEntity($data = null, array $options = [])
 * @method \FrontEnds\Model\Entity\Setting[] newEntities(array $data, array $options = [])
 * @method \FrontEnds\Model\Entity\Setting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FrontEnds\Model\Entity\Setting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FrontEnds\Model\Entity\Setting[] patchEntities($entities, array $data, array $options = [])
 * @method \FrontEnds\Model\Entity\Setting findOrCreate($search, callable $callback = null)
 */class SettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('settings');
        $this->displayField('name');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->allowEmpty('name');
        $validator
            ->allowEmpty('value');
        return $validator;
    }
}
