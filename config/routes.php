<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */

    $routes->connect('/', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'index']);
    $routes->connect('/register', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'register']);
    
    //DIGIT SPAN =================
    $routes->connect('/instruction_trial_digit_span', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialDigitSpan']);
    $routes->connect('/stage_trial_digit_span', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialDigitSpan']);

    $routes->connect('/instruction_digit_span', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionDigitSpan']);
    $routes->connect('/stage_digit_span', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageDigitSpan']);
 
    //3D Rotation =================
    $routes->connect('/instruction_trial_3d_rotation', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrial3dRotation']);
    $routes->connect('/stage_trial_3d_rotation', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrial3dRotation']);

    $routes->connect('/instruction_3d_rotation', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instruction3dRotation']);
    $routes->connect('/stage_3d_rotation', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stage3dRotation']);

    //Line Combination =================
    $routes->connect('/instruction_trial_line_combination', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialLineCombination']);
    $routes->connect('/stage_trial_line_combination', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialLineCombination']);

    $routes->connect('/instruction_line_combination', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionLineCombination']);
    $routes->connect('/stage_line_combination', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageLineCombination']);

    //Paper Folding =================
    $routes->connect('/instruction_trial_paper_folding', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialPaperFolding']);
    $routes->connect('/stage_trial_paper_folding', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialPaperFolding']);

    $routes->connect('/instruction_paper_folding', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionPaperFolding']);
    $routes->connect('/stage_paper_folding', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stagePaperFolding']);

    //Building Memory =================
    $routes->connect('/instruction_trial_building_memory', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialBuildingMemory']);
    $routes->connect('/stage_trial_building_memory', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialBuildingMemory']);

    $routes->connect('/instruction_building_memory', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionBuildingMemory']);
    $routes->connect('/stage_building_memory', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageBuildingMemory']);

    //Shape Classification =================
    $routes->connect('/instruction_trial_shape_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialShapeClassification']);
    $routes->connect('/stage_trial_shape_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialShapeClassification']);

    $routes->connect('/instruction_shape_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionShapeClassification']);
    $routes->connect('/stage_shape_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageShapeClassification']);

    //Figural Classification =================
    $routes->connect('/instruction_trial_figural_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialFiguralClassification']);
    $routes->connect('/stage_trial_figural_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialFiguralClassification']);

    $routes->connect('/instruction_figural_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionFiguralClassification']);
    $routes->connect('/stage_figural_classification', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageFiguralClassification']);

    //Deret Angka =================
    $routes->connect('/instruction_trial_deret_angka', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionTrialDeretAngka']);
    $routes->connect('/stage_trial_deret_angka', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageTrialDeretAngka']);

    $routes->connect('/instruction_deret_angka', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'instructionDeretAngka']);
    $routes->connect('/stage_deret_angka', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageDeretAngka']);
    
    // =================
    $routes->connect('/stage_end', ['plugin' => 'FrontEnds', 'controller' => 'Pages', 'action' => 'stageEnd']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    // $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();


