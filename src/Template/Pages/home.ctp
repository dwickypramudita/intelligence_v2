<!DOCTYPE html>
<html lang="en">
<head>
     <?= $this->Html->meta('icon') ?>

    <!-- Bootstrap and OneUI CSS framework -->
    <?php
        echo $this->Html->css([
            '/css/bootstrap.min.css',
            '/css/bootstrap.min.css',
            '/css/header.css',
            '/css/slick.min.css',
            '/css/slick-theme.min.css',
            '/css/oneui.css']);
    ?>

    <?php 
        echo $this->Html->script([
        '/js/jquery.min.js',
        '/js/bootstrap.min.js', 
        '/js/jquery.slimscroll.min.js',
        '/js/jquery.scrollLock.min.js',
        '/js/jquery.appear.min.js',
        '/js/jquery.countTo.min.js',
        '/js/jquery.placeholder.min.js',
        '/js/js.cookie.min.js',
        '/js/app.js',
        '/js/bootstrap-datepicker.min.js',        
        
        '/js/jquery.validate.min.js',
        '/js/plugins/slick/slick.min.js'
                 

        ]); 
    ?>  

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?= $this->element('header') ?>
    <!-- Page Content -->
    <div id="content" class="container">
        <?= $this->Flash->render() ?>
        <div class="row">
            <?= $this->fetch('content') ?>
        </div>
    </div>
    <?= $this->element('footer') ?>
</body>
</html>