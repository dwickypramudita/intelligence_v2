<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework!!!';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php
        echo $this->Html->css([
            '/webroot/css/bootstrap.min.css',
            '/webroot/css/bootstrap.min.css',
            '/webroot/css/header.css',
            '/webroot/css/slick.min.css',
            '/webroot/css/slick-theme.min.css',
            '/webroot/css/oneui.css']);
    ?>

    <?php 
        echo $this->Html->script([
        // '/webroot/js/jquery.min.js',
        // '/webroot/js/bootstrap.min.js', 
        // '/webroot/js/jquery.slimscroll.min.js',
        // '/webroot/js/jquery.scrollLock.min.js',
        // '/webroot/js/jquery.appear.min.js',
        // '/webroot/js/jquery.countTo.min.js',
        // '/webroot/js/jquery.placeholder.min.js',
        // '/webroot/js/js.cookie.min.js',
        // '/webroot/js/app.js',
        // '/webroot/js/bootstrap-datepicker.min.js',        
        // '/webroot/js/jquery.validate.min.js',
        // '/webroot/js/plugins/slick/slick.min.js'

        '/webroot/js/application.js',
        '/webroot/js/bootstrap.js',
        '/webroot/js/camera.js',
        '/webroot/js/jquery.easing.1.3.js',
        '/webroot/js/jquery.isotope.min.js',
        '/webroot/js/jquery.jcaraousel.js',
        '/webroot/js/jquery.mobile.customized.min.js',
        '/webroot/js/jquery.preloader.js',
        '/webroot/js/jquery.prettyPhoto.js',
        '/webroot/js/jquery.tweet.js',
        '/webroot/js/myscript.js',
        '/webroot/js/sorting.js',
        '/webroot/js/superfish.js'
        ]); 
    ?>  

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <li><a target="_blank" href="http://book.cakephp.org/3.0/">Documentation</a></li>
                <li><a target="_blank" href="http://api.cakephp.org/3.0/">API</a></li>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
