<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900|Roboto:100,300,400,500,700,900" rel="stylesheet" />
<style type="text/css">
    p, h1, h2, h3, h4, h5 {
        margin: 0;
    }

    body{
        margin: 0;
        padding: 0;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        font-size: 12px;
        background: -webkit-linear-gradient(left, #3931af, #00c6ff);
        color: white;
    }

    .error-wrapper {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .error-wrapper .title {
        font-size: 32px;
        font-weight: 700;
        color: black;
    }

    .error-wrapper .info {
        font-size: 14px;
    }

    .home-btn, 
    .home-btn:focus, 
    .home-btn:hover, 
    .home-btn:visited {
        text-decoration: none;
        font-size: 14px;
        color: #55aa29;
        padding: 17px 77px;
        border: 1px solid #55aa29;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -o-border-radius: 3px;
        border-radius: 3px;
        display: block;
        margin: 20px 0;
        width: max-content;
        background-color: transparent;
        outline: 0;
    }

    /*.man-icon {
        background: url('<?php echo $this->Url->build("/img/error.png"); ?>') center center no-repeat;
        display: inline-block;
        height: 100px;
        width: 118px;
        margin-bottom: 16px;
    }*/

</style>
<div class="container">
    <div class="error-wrapper">
        <div ><img src="<?php echo $this->Url->build("/img/error.png"); ?>" style="height: 100px";width:100px;></div>
        <h3 class="title">ERROR</h3>
        <p class="info">
            <br>
            <?php echo $message ?>
            <br>
            <br>
            Catatan :
            <br>- Apabila anda sedang mengerjakan tes atau akan mengerjakan tes tahap 2 dan muncul halaman ini, pastikan koneksi internet tersambung dan tekan refresh pada browser. Kembali ke halaman awal akan menghapus data tes anda dan anda harus mengulangi dari awal. <br>
            <br>- Apabila anda mengetik url secara manual dan terjadi error ini, silahkan kembali ke halaman awal dengan klik link <a href="<?php echo $this->Url->build('/'); ?>" style="color:black;"> ini.</a>
        </p>
    </div>
</div>