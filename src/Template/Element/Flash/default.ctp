<!-- <?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
?>
<div class="<?= h($class) ?>"><?= h($message) ?></div>
 -->

<?php 
$icon = '';
switch ($params['class']) {
	case 'success':
		$icon = 'fa fa-check-circle-o';
		break;
	case 'danger':
		$icon = 'fa fa-warning';
		break;
	case 'warning': 
		$icon = 'fa fa-times-circle-o';
		break;
	case 'info': 
		$icon = 'fa fa-info';
		break;
}
?>
<div role="alert" class="alert pull-right alert-<?php echo h($params['class']) ?> alert-dismissable " style = 'display: block;float:right;width: 400px;'>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h3 class="font-w300 push-15"><i class = '<?php echo $icon ?>'></i> &nbsp;<?php echo h($params['title']); ?></h3>
    <p><?php echo h($message) ?></p>
</div>
